<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use  App\User;
use  App\UserFriend;

$factory->define(UserFriend::class, function (Faker $faker) {
        //buscar um user aleatorio para o id_user
        $user = User::all()->random()->id;

        //buscar um user aleatorio para o id_friend
        $id_friend = User::all()->random()->id;
        
        //verificar que o friend nao é ele mesmo
        if ($id_friend != $user){
            $friend =$id_friend;
        }
        else 
        $friend = User::all()->random()->id;

        //var_dump($user);
        //var_dump($friend);

        return [

        'user_id'    =>  $user, 
        'friend_id'  => $friend,
        'accept'     => true,
        'date_begin' => $faker->dateTimeBetween($startDate = '-2 years', $endDate = 'now')

        
    ];
});
