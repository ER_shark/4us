<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\GroupPublication;
use App\Publication;
use App\Group;
use Faker\Generator as Faker;

$factory->define(GroupPublication::class, function (Faker $faker) {
    $publ = Publication::all()->random()->id;
    $group  = Group::all()->random()->id;
    
    return [
        'publication_id' => $publ,
        'group_id'       => $group
    ];
});
