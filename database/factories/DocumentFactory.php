<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Type;
use App\Publication;
use App\Document;
use App\User;
use Faker\Generator as Faker;

$factory->define(Document::class, function (Faker $faker) {
//buscar um user aleatorio para o id_user
$type = Type::all()->random()->id;
$publ = Publication::all()->random()->id;
$user = User::all()->random()->id;

    return [
           'type_id'        => $type,
           'publication_id' => $publ,
           'user_id'        => $user,
           'name'           => $faker->name,
           'date'   => $faker->dateTimeBetween($startDate = '-2 years', $endDate = 'now'),
           'path'           =>'c:\\'. implode('\\', $faker->words($faker->numberBetween(0, 3))),
    ];
});
