<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Type;
use Faker\Generator as Faker;

$factory->define(Type::class, function (Faker $faker) {
    //arrays de tipos de files
    $type =  ['*.exe','*.rar', '*.pdf', '*.doc','*.jpg'];

    return [
        'type_doc' => $type[rand(0,4)],
    ];
});
