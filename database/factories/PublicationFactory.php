<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Publication;
use App\Category;
use App\User;
use Faker\Generator as Faker;

$factory->define(Publication::class, function (Faker $faker) {
    //array de tipos de visibilidade
    $visibility=['public', 'friend', 'private','group'];

    //aceder aos ids
    $user=User::all()->random()->id;

    return [
        'user_id'     => $user,
        'category_id' => 1,
        'description' => $faker->realText,
        'accept'      => 1,
        'date'        => $faker->dateTimeBetween($startDate = '-2 years', $endDate = 'now'),
        'visibility'  => $visibility[rand(0,3)],
    ];
});
