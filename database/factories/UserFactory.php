<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/


$factory->define(User::class, function (Faker $faker) {
    //array de indicativos de redes portuguesas
    $network = ['+351 91', '+351 92','+351 93','+351 96'];

    return [
            'role_id'           => $faker->numberBetween(2,4) ,
            'user_name'         => $faker->username,
            'name'              => $faker ->name,
            'email'             => $faker->unique()->safeEmail,
            'email_verified_at' => $faker->dateTimeBetween($startDate = '-2 years', $endDate = 'now'),
            'password'          => Hash::make('password'),
            'points'            => 0,
            'phone'             => $network[rand(0,3)].rand( 1000000 , 9999999 ),
            'description'       => $faker->realText,
            'remember_token'    => str_random(10),
    ];
});
