<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\GroupUser;
use App\User;
use App\Group;
use Faker\Generator as Faker;

$factory->define(GroupUser::class, function (Faker $faker) {
    //array de roles
    $role =['member', 'owner', 'admin','professor'];
    //aceder aos ids
    $user=User::all()->random()->id;
    $group=Group::all()->random()->id;

    return [
        'user_id' => $user,
        'group_id'=> $group,
        'roles'   => $role[rand(0,3)],
    ];
});
