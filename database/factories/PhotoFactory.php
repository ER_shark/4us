<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Photo;
use Faker\Generator as Faker;

$factory->define(Photo::class, function (Faker $faker) {
    static $number = 1;
    $photo =  ['images\aiony-haust-S-igcQ6qpg4-unsplash.jpg',
    'images\alexander-krivitskiy-3FvwtxjkICw-unsplash.jpg',
    'images\brooke-cagle-kvKSL7B6eTo-unsplash.jpg',
    'images\cristian-newman-ogXv77BAL5s-unsplash.jpg',
    'images\joey-nicotra-erGcMFMaYGk-unsplash.jpg',
    'images\lucas-vicente-bPLs2ez1IiM-unsplash.jpg',
    'images\raychan-wIkhjWYGfME-unsplash.jpg',
    'images\suad-kamardeen-pn2-F5lKM-U-unsplash.jpg',
    'images\tamara-bellis-oj2nLF70ya4-unsplash.jpg',
    'images\taylor-T6hnWTfK2VU-unsplash.jpg',
    'images\tiko-giorgadze-YevDFvPJ8uQ-unsplash.jpg'
];

    return [
        'url'   => $photo[$number-1],
        'user_id' =>$number++,
        'galery'=>'profile',
        
    ];
  
});
