<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use App\User;
use App\UserDegree;


$factory->define(UserDegree::class, function (Faker $faker) {
    //array dos enums de degree
    $degree =  ['Outro','Estudante', 'Licenciado', 'Mestrado','Doutorado'];
    
    //buscar um user aleatorio
    $user =User::all()->random();
    
    //aceder ao id desse user
    $user_id = $user->id;
   
    //aceder ao role desse mesmo user
    $role = $user->id_role;

    //verificar os roles para colocar o grau academico que faz sentido
    if ($role == 3){
        $degrees = $degree[0];
    }
    if ($role == 1){
        $degrees = $degree[rand(1,4)];
    }
    if ($role == 2){
        $degrees = $degree[rand(3,4)];
    }
    else{
        $degrees = $degree[rand(0,4)];
    }
    //var_dump($role);
    //var_dump($degrees);

    return [
        //User::all()->random()->user_id

        'user_id'   => $user_id,
        'degrees'   => $degrees,
        'date_begin'=> $faker->dateTimeBetween($startDate = '-10 years', $endDate = 'now'),
        'date_end'  => $faker->dateTimeBetween($startDate = '-6 years', $endDate = 'now')
    ];

 
});
