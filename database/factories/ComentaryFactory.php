<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Publication;
use App\User;
use App\Comentary;
use Faker\Generator as Faker;

$factory->define(Comentary::class, function (Faker $faker) {
    $user = User::all()->random()->id;
    $publ = Publication::all()->random()->id;

    return [
    'publication_id' => $publ,
    'user_id'        => $user,
    'comentary'      => $faker->realText,
    'date'        => $faker->dateTimeBetween($startDate = '-2 months', $endDate = 'now'),
    ];
});
