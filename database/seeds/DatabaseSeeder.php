<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(RolesTableSeeder::class);
         $this->call(UsersTableSeeder::class);
         $this->call(UserDegreesTableSeeder::class);
         $this->call(UserFriendsTableSeeder::class);
         $this->call(TypesTableSeeder::class);
         $this->call(PublicationsTableSeeder::class);
         $this->call(DocumentsTableSeeder::class);
         $this->call(GroupsTableSeeder::class);
         $this->call(GroupUsersTableSeeder::class);
         $this->call(GroupPublicationsTableSeeder::class);
         $this->call(ComentariesTableSeeder::class);
         $this->call(PhotoTableSeeder::class);


    }
}
