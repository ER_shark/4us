<?php

use Illuminate\Database\Seeder;

class UserFriendsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\UserFriend::class, 10)->create();

    }
}
