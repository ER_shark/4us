<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon;

use App\GroupUser;
use App\Group;


class GroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {



        DB::transaction(function () { // Transação para obrigar a fazer tudo ou nada
 
            // Criar o grupo de ocorrências da universidade (onde são partilhadas mudanças, eventos, informações)
            Group::create([
                'name' => "Ocorrências Universidade",
                'description' => "Grupo onde são partilhadas todas as ocorrências da universidade, desde problemas, novas informações da universidade, mudança de aulas, etc.",
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
            

            // O primeiro grupo (id = 1) é Ocorrências Universidade, onde o dono é o admin (também tem id = 1)
            GroupUser::create([
                'user_id' => 1,
                'group_id' => 1,
                'roles' => 'owner',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
            
        });


        factory(App\Group::class, 5)->create();
    }
}
