<?php

use Illuminate\Database\Seeder;


class UserDegreesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\UserDegree::class, 20)->create();
    }
}
