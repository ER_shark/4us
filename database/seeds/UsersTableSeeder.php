<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     

        User::create([
            'role_id'=> 1 ,
            'user_name'=>'admin',
            'name'=>'admin',
            'email'=>'admin@admin.com',
            'password' => Hash::make('admin'),
            'points'=> 0,
            'phone' =>'+351 96'.rand ( 1000000 , 9999999 ),
            'description'=> 'admin',
            'remember_token' => str_random(10),
        ]);

        factory(App\User::class, 10)->create();
    }
}
