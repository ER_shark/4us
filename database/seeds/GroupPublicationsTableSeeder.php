<?php

use Illuminate\Database\Seeder;

class GroupPublicationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\GroupPublication::class, 20)->create();
    }
}
