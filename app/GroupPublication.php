<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupPublication extends Model
{
    protected $table = 'group_publications';

    public function group(){

        return $this->belongsTo('App\Group');
    }

    public function publication(){

        return $this->belongsTo('App\Publication');
    }
}
