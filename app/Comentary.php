<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comentary extends Model
{
    protected $table = 'comentaries';
    protected $fillable = ['publication_id','user_id','comentary','date'];

    public function user(){

        return $this->belongsTo('App\USer');
    }
    public function publication(){

        return $this->belongsTo('App\Publication');
    }
}
