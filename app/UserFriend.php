<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class UserFriend extends Model
{
    protected $table = 'user_friends';

    public function user(){

        return $this->belongsTo('App\User');
    }

    public function friend(){

        return $this->belongsTo('App\User');
    }

    
}
