<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $table = 'groups';

    public function userGroups(){

        return $this->hasMany('App\GroupUser');
    }

    public function groupPublications(){

        return $this->hasMany('App\GroupPublication');
    }


}
