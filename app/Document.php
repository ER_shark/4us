<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $table = 'documents';

    public function type(){

        return $this->belongsTo('App\Type');
    }
    public function publication(){

        return $this->belongsTo('App\Publication');
    }

    public function userDocuments(){

        return $this->hasOne('App\UserDocument');
    }
}
