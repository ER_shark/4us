<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Publication extends Model
{
    
    protected $table = 'publications';

    public function user(){

        return $this->belongsTo('App\User');
    }

    public function category(){

        return $this->belongsTo('App\Category');
    }

    public function documents(){

        return $this->hasMany('App\Document');
    }

    public function groupPublication(){

        return $this->hasMany('App\GroupPublication');
    }


    public function comentary(){

        return $this->hasMany('App\Comentary');
    }


    public function likes()
    {
        // return $this->belongsToMany('App\User', 'publication_like', 'publication_id', 'user_id');
        return $this->hasMany('App\PublicationLike');
    }

}
