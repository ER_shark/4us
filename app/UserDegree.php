<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDegree extends Model
{
    protected $table = 'user_degrees';

    public function user(){

        return $this->belongsTo('App\User');
    }
}
