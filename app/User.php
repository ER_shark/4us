<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function role(){

        return $this->belongsTo('App\Role');
    }

    public function userDegrees(){

        return $this->hasMany('App\UserDegree');
    }

    public function userFriends(){

        return $this->hasMany('App\UserFriend');
    }

    
    public function userDocuments(){

        return $this->hasMany('App\Document');
    }

    public function userGroups(){

        return $this->hasMany('App\GroupUser');
    }


    public function userPublications(){

        return $this->hasMany('App\Publication');
    }

    public function comentaries(){

        return $this->hasMany('App\Comentary');
    }

    public function photos(){

        return $this->hasMany('App\Photo');
    }



    public function likes()
    {
        // return $this->belongsToMany('App\Publication', 'publication_like', 'user_id', 'publication_id');
        return $this->hasMany('App\PublicationLike');
    }

}
