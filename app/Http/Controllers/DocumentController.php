<?php

namespace App\Http\Controllers;

use App\Document;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;


use File;

use App\Http\Controllers\UserController;
use App\Http\Controllers\PhotoController;
use App\Http\Controllers\GoogleDriveController;
use App\Http\Controllers\UserFriendController;



class DocumentController extends Controller
{



    // Este método permite fazer download de um documento, do google drive
    public function downloadDocument($filename) {

        $googledrive = new GoogleDriveController(); // Instancia o controlador para ir buscar o método do download

        return $googledrive->getFile($filename); // Faz download  do ficheiro
        
    }




    // Este método permite criar um novo documento
    public function createDocument($doc, $publication, $current_user, $googledrive) {


        $document = new Document; // Cria nova instância do modelo Document (novo documento)
        $filename = $doc->getClientOriginalName(); // Vai buscar o nome do ficheiro
        $filename = time().$filename; // Adicionar timestamp ao nome do ficheiro para evitar ficheiros com mesmo nome
        

        // Tipo de documento dependerá essencialmente da extensão do ficheiro
        $document->type_id = 1;
        $document->publication_id = $publication->id; // Id da publicação é a publicação criada anteriormente
        $document->user_id = $current_user->id; // Quem fez upload do documento é o utilizador que está logado
        $document->date = Carbon::now(); // Data atual
        $document->name = $filename; // Nome do ficheiro
        $document->path = $filename; // Localização do ficheiro


        if ($doc) {

            $doc->move('docs/', $filename); // Guarda a foto no caminho especificado, temporariamente
            
            // Guarda na pasta files do google drive a fotografia guardada temporiariamente
            $googledrive->putFile('docs/', $filename);

            // Remove a fotografia guardada temporariamente, após ter sido guardada no google drive
            File::delete(public_path("docs/" . $filename)); // public_path devolve o caminho completo na pasta public

        }


        $document->save(); // Guarda o documento na base de dados (guarda o registo)


    }



    /**
     * Este método permite mostrar todos os documentos inseridos por um determinado utilizador.
     */
    public function showDocuments($username = null) { // Se não for enviado username no URL, assume null

        $current_user = Auth::user();

        // Instanciar controlador para utilizar método de outro controlador
        $photoController = new PhotoController();
        $userController = new UserController();
        $userFriendController = new UserFriendController();


        // Vai buscar o utilizador responsável pelo perfil (cujo username está no url)
        $myself = $userController->getUserByUsername($username);

        // Verifica se o utilizador do perfil é amigo do utilizador atual
        $isFriend = $userFriendController->isFriendOf($myself, $current_user);


        if ($myself) {

            if ($isFriend == 1 || $myself->id == $current_user->id) { // Se for amigo dele ou for ele próprio, pode ver os amigos
                
                // Buscar documentos do utilizador $myself (dono do perfil)
                $documents = $myself->userDocuments;

                // Buscar fotografia de perfil do utilizador com id $myself->id
                $profile = $photoController->getProfilePhoto($current_user->id);

                return view('test2.documents', compact('current_user', 'myself', 'profile', 'documents'));
            } else {
                // Se não for amigo, volta para a página anterior
                return redirect()->back();
            }




        } else {
            return view('auth.login');
        }

    }





    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function show(Document $document)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function edit(Document $document)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Document $document)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Document::destroy($id);
        return redirect('/home');
    }
}

