<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Storage;

class GoogleDriveController extends Controller
{

    // Buscar um ficheiro armazenado no google drive
    public function getFile($filename) {

        $dir = '/';
        $recursive = false; // Get subdirectories also?
        $contents = collect(Storage::disk('google')->listContents($dir, $recursive));
        $file = $contents
            ->where('type', '=', 'file')
            ->where('filename', '=', pathinfo($filename, PATHINFO_FILENAME))
            ->where('extension', '=', pathinfo($filename, PATHINFO_EXTENSION))
            ->first(); // Caso hajam vários ficheiros com o mesmo nome (dificil porque usamos timestamp)

            $rawData = Storage::disk('google')->get($file['path']);

        return response($rawData, 200)
            ->header('Content-Type', $file['mimetype'])
            ->header('Content-Disposition', "attachment; filename='$filename'");

    }




    /**
     * Este método permite buscar uma fotografia do google drive para visualizar
     */
    public function getImageLink($filename) {

        $dir = '/';
        $recursive = false; // Get subdirectories also?
        $contents = collect(Storage::disk('google')->listContents($dir, $recursive));
        $file = $contents
            ->where('type', '=', 'file')
            ->where('filename', '=', pathinfo($filename, PATHINFO_FILENAME))
            ->where('extension', '=', pathinfo($filename, PATHINFO_EXTENSION))
            ->first(); // Caso hajam nomes repetidos (dificilmente acontece porque usamos timestamps)
        
        $id = $file['path'];
        
        return 'http://drive.google.com/uc?id=' . $id;
    }


    // Criar um ficheiro no google drive
    public function createFile() {

        Storage::disk('google')->put('test.txt', 'Hello World');
        return 'File was created in Google Drive';

    }



    // Armazenar um ficheiro no google drive
    public function putFile($path, $filename) {

        $filePath = public_path($path . $filename);
        Storage::disk('google')->put($filename, fopen($filePath, 'r+'));

        return 'File was saved to Google Drive';

    }


    // Ler conteúdo do google drive
    public function filesList() {

        $dir = '/';
        $recursive = false; // Get subdirectories also?
        $contents = collect(Storage::disk('google')->listContents($dir, $recursive));
        //return $contents->where('type', '=', 'dir'); // directories
        return $contents->where('type', '=', 'file'); // files

    }


}
