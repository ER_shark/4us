<?php

namespace App\Http\Controllers;

use App\UserFriend;
use App\Photo;
use App\User;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


use App\Http\Controllers\PhotoController;


class UserFriendController extends Controller
{
    /**
     * Este método mostra os amigos de um utilizador
     */
    public function getFriends($username = null)
    {

        $photoController = new PhotoController();
        $userController = new UserController();

        $current_user = Auth::user();
        
        // Vai buscar o utilizador responsável pelo perfil (cujo username está no url)
        $myself = $userController->getUserByUsername($username);

        $isFriend = $this->isFriendOf($myself, $current_user);

        $option = 1; // Apenas mostrar amigos (não é pesquisa de utilizadores)

        if ($myself) {

            if ($isFriend == 1 || $myself->id == $current_user->id) { // Se for amigo dele ou for ele próprio, pode ver os amigos
                $friends = $this->findFriends($myself);
                $profile = $photoController->getProfilePhoto($current_user->id);
                return view('test2.friends', compact('current_user', 'friends', 'profile', 'myself', 'option'));
            } else {
                // Se não for amigo, volta para a página anterior
                return redirect()->back();
            }

        } else {
            return view('auth.login');
        }
    }






    /**
     * Este método devolve o id do pedido de amizade enviado, do próprio utilizador para outro utilizador
     */
    public function getRequest ($user, $current_user, $option) {

        if ($option == 1) {
            // Buscar todas os pedidos de amizade enviados
            $friendRequests = $this->getSentRequests();
        } else if ($option == 2) {
            // Buscar todas os pedidos de amizade recebidos
            $friendRequests = $this->getFriendRequests();
        }

        $requestID = 0;

        // Percorre os pedidos de amizade enviados para saber qual o id
        foreach($friendRequests as $request) {

            if ($option == 1) { // Pedidos de amizade enviados

                if ($request->friend_id == $user->id && $request->user_id == $current_user->id && $request->accept == 0) {
                    $requestID = $request->id;
                }

            } else if ($option == 2) { // Pedidos de amizade recebidos

                if ($request->friend_id == $current_user->id && $request->user_id == $user->id && $request->accept == 0) {
                    $requestID = $request->id;
                }

            }

        }

        return $requestID;
        
    }

    






    /**
     * Este método indica se o utilizador logado é amigo de um determinado utilizador
     */
    public function isFriendOf($friendUser, $current_user) {

        $isFriend = 0;
        
        $friends = $this->findfriends($friendUser);

        foreach($friends as $friend) {

            if ($friend['id'] == $current_user->id) {
                $isFriend = 1;
            }

        }


        if ($isFriend == false) {

            $sent_requests = $this->getSentRequests(); // Buscar pedidos de amizade enviados


            // Percorre os pedidos de amizade enviados para saber se não é amigo mas enviou pedido de amizade (e está á espera da resposta)
            foreach($sent_requests as $request) {

                if ($request->friend_id == $friendUser['id']) {
                    $isFriend = 5;
                }

            }



            $friend_requests = $this->getFriendRequests(); // Buscar pedidos de amizade recebidos
            // Percorre os pedidos de amizade recebidos para saber se não é amigo mas recebeu pedido de amizade (e ainda não aceitou)
            foreach($friend_requests as $request) {
                
                if ($request->user_id == $friendUser['id']) {
                    $isFriend = 6;
                }

            }

        }


        return $isFriend;

    }





    /**
     * Esta função permite aceitar um pedido de amizade
     */
    public function acceptRequest($requestID) {

        $request = UserFriend::find($requestID); // Vai buscar o pedido de amizade com este id
        $request->accept = 1; // Pedido aceite
        $request->save(); // Guarda alterações

        $current_user = Auth::User(); // Utilizador logado


        $user = $request->user_id; // Utilizador que enviou o pedido de amizade


        // Cria uma nova amizade (tem de ter 2 registos para cada amizade - 1 é amigo do 2 e 2 é amigo do 1)
        $friend = new UserFriend;
        $friend->user_id = $current_user->id; // Utilizador logado
        $friend->friend_id = $user; // Utilizador que enviou o pedido de amizade
        $friend->accept = 1; // Está aceite
        $friend->date_begin = Carbon::now(); // Data atual
        $friend->save(); // Guardar alterações na base de dados


        return redirect()->back(); // Redireciona de volta para a página anterior

    }



    /**
     * Esta função permite aceitar um pedido de amizade
     */
    public function declineRequest($requestID) {

        $request = UserFriend::find($requestID); // Vai buscar o pedido de amizade com este id
        $request->accept = -1; // Pedido recusado
        $request->save(); // Guarda alterações

        return redirect()->back();

    }





    /**
     * Esta função permite cancelar um pedido de amizade enviado
     */
    public function cancelRequest($requestID) {

        $request = UserFriend::find($requestID); // Vai buscar o pedido de amizade com este id
        $request->forceDelete(); // Apaga o pedido de amizade enviado

        return redirect()->back(); // Redireciona de volta

    }




    /**
     * Este método mostra os pedidos de amizade enviados e recebidos
     */
    public function showFriendRequests() {

        $friendRequests = [];
        $sentRequests = [];
        
        $photoController = new PhotoController(); // Instanciar controlador para usar método de ir buscar foto de perfil

        $current_user = Auth::User(); // Vai buscar utilizador logado

        if ($current_user) { // Se o utilizador estiver logado

            $profile = $photoController->getProfilePhoto($current_user->id); // Buscar foto de perfil para preencher a imagem do lado direito (no menu)

            $friend_requests = $this->getFriendRequests(); // Buscar pedidos de amizade recebidos
            $sent_requests = $this->getSentRequests(); // Buscar pedidos de amizade enviados

            // Vai percorrer todos os pedidos para guardar num array, juntamente com dados do utilizador que enviou o pedido
            foreach($friend_requests as $request) {

                $userWhoSentRequest = User::find($request->user_id);
                $friendRequests["request_" . $request->id] = $request; // Dados do pedido de amizade
                $friendRequests["request_" . $request->id]["userWhoSentRequest"] = $userWhoSentRequest; // Utilizador que fez o pedido
                $friendRequests["request_" . $request->id]["userImage"] = $photoController->getProfilePhoto($userWhoSentRequest->id); // Foto do utilizador que fez o pedido

            }


            // Vai percorrer todos os pedidos para guardar num array, juntamente com dados do utilizador para quem enviamos o pedido
            foreach($sent_requests as $request) {

                $user = User::find($request->friend_id);
                $sentRequests["request_" . $request->id] = $request; // Dados do pedido de amizade enviado
                $sentRequests["request_" . $request->id]["user"] = $user; // Utilizador para quem enviamos o pedido
                $sentRequests["request_" . $request->id]["userImage"] = $photoController->getProfilePhoto($user->id); // Foto do utilizador para quem enviamos o pedido

            } 

            return view('test2.friend_requests', compact('current_user', 'profile', 'friendRequests', 'sentRequests'));
        } else {
            return view('auth.login');
        }

    }



    /**
     * Permite encontrar os amigos do utilizador enviado como argumento
     */
    public function findFriends($user)
    {

        $allFriends = [];
        $friends = $user->userFriends;

        if ($user) {

            // Percorre todos os amigos do utilizador (da tabela user_friend)
            foreach($friends as $friend) {

                // Se o pedido de amizade foi aceite
                if ($friend->accept == 1) {

                    // Vai buscar o utilizador que tem o id do amigo dele
                    $friend = User::find($friend->friend_id);

                    $allFriends["friend_$friend->id"] = $this->findFriendDetails($friend);

                }

            }

        }

        return $allFriends;
    }


    /**
     * Preenche o array com os dados dos seus amigos
     */
    public function findFriendDetails($friend)
    {
        $photoController = new PhotoController();
        return [
            'photo' => $photoController->getProfilePhoto($friend->id),
            'id' => $friend->id,
            'name' => $friend->name,
            'description' => $friend->description,
            'user_name' => $friend->user_name
        ];
    }




    /**
     * Este método permite enviar um pedido de amizade
     */
    public function sendFriendRequest($friendId)
    {
        $current_user = Auth::user();

        if ($current_user) {

            $friend = new UserFriend;
            $friend->user_id =  $current_user->id;
            $friend->friend_id =  $friendId;
            $friend->date_begin  = Carbon::now(); // Data atual
            // accept = 0 (ainda não aceite), accept = -1 (recusado), accept = 1 (aceite)
            $friend->accept =  0; // Quando o pedido de amizade é enviado, não foi aceite ainda. Caso seja aceite, é alterado de 0 para 1
            $friend->save();

            return redirect()->back(); // Volta para a página anterior
        } else {
            return redirect('/');
        }
    }




     /**
     * Este método permite remover um amigo
     */
    public function removeFriend($friendId)
    {
        $current_user = Auth::user();

        if ($current_user) {

            $friends = UserFriend::All();

            foreach($friends as $friend) {
                if (($friend->user_id == $friendId && $friend->friend_id == $current_user->id) || ($friend->user_id == $current_user->id && $friend->friend_id == $friendId)) {
                    $friend->forceDelete(); 
                }
            }

            return redirect()->back(); // Volta para a página anterior

        } else {
            return redirect('/');
        }
    }





    /**
     * Permite aceitar um pedido de amizade recebido.
     */
    public function acceptDeclineFriendRequest($userId, $accept)
    {
        
        $current_user = Auth::User();

        if ($current_user) {

            // Vai buscar o id do pedido
            $requestId = $this->getRequestID($userId);

            // Retorna exceção caso não encontre
            $friend = UserFriend::findOrFail($requestId); // Vai buscar o pedido com o id $requestId

            if ($accept == 1) {
                $friend->accept = 1; // Altera o accept para 1 (pedido aceite);
            } else {
                $friend->accept = -1; // Altera o accept para -1 (pedido recusado);
            }

            $friend->save(); // Guarda essa alteração


            return redirect()->back(); // Redireciona de volta

        } else {
            return redirect('/');
        }
    }




    /**
     * Este método devolve o id do pedido de amizade enviado pelo utilizador enviado como argumento, para o utilizador logado.
     * Permite auxiliar o método acceptFriendRequest.
     */
    public function getRequestID($userId) {

        $allFriends = UserFriend::all(); // Todos as relações de amizade e pedidos de amizade existentes
        $current_user = Auth::User(); // Utilizador que está logado

        if ($current_user) {

            // Percorre todos as relações de amizade e pedidos de amizade existentes
            foreach($allFriends as $friend) {

                // Se um pedido de amizade ainda não tiver sido aceite (accept = 0) e tenha sido enviado do utilizador $userId para o utilizador logado ($current_user)
                if ($friend->accept == 0 && $friend->friend_id == $current_user->id && $friend->user_id == $userId) {
                    return $friend->id; // Devolve o id desse pedido de amizade
                }

            }
            
        }

    }




    /**
     * Este método retorna uma lista com os pedidos de amizade que um determinado utilizador recebeu (e ainda não aceitou/recusou).
     */
    public function getFriendRequests() {
     
        // Vai buscar todos os amigos e pedidos de amizade existentes
        // $userFriends = Auth::user()->userFriends; 
        $allFriends = UserFriend::all();

        // Vai filtrar para devolver apenas os que não foram aceites (accept = 0) e que foram enviados para o utilizador logado
        $users = $allFriends->filter(function($user)
        {
            $current_user = Auth::User();
            return $user->accept == 0 && $user->friend_id == $current_user->id; // Condição que tem de ser verificada para devolver esse registo da lista
        });

        return $users;

    }





    /**
     * Este método retorna uma lista com os pedidos de amizade que um determinado utilizador enviou (e ainda não foram aceites/recusados).
     */
    public function getSentRequests() {
     
        // Vai buscar todos os amigos e pedidos de amizade existentes
        $allFriends = UserFriend::all();

        // Vai filtrar para devolver apenas os que não foram aceites (accept = 0) e que foram enviados pelo utilizador logado
        $users = $allFriends->filter(function($user)
        {
            $current_user = Auth::User();
            return $user->accept == 0 && $user->user_id == $current_user->id; // Condição que tem de ser verificada para devolver esse registo da lista
        });

        return $users;

    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserFriend  $userFriend
     * @return \Illuminate\Http\Response
     */
    public function show(UserFriend $userFriend)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserFriend  $userFriend
     * @return \Illuminate\Http\Response
     */
    public function edit(UserFriend $userFriend)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserFriend  $userFriend
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserFriend $userFriend)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserFriend  $userFriend
     * @return \Illuminate\Http\Response
     */
    public function destroy($friend_id)
    {
        //encontra qual o id da amizade
        $table_id = $this->findFriend($friend_id);
        UserFriend::destroy($table_id);
        return redirect()->back(); //retorna
    }
}
