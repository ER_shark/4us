<?php

namespace App\Http\Controllers;

use App\User;
use App\Photo;
use App\UserDegree;
use App\GroupPublication;
use App\Publication;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;


use App\Http\Controllers\UserFriendController;
use App\Http\Controllers\PhotoController;
use App\Http\Controllers\RoleController;



class UserController extends Controller
{


        
    // Este método permite buscar o utilizador responsável pelo perfil (cujo username está no url)
    public function getUserByUsername($username) {
        
        // Se está a visitar o perfil de alguém
        if ($username != null) {

            $profileId = null;

            // Vai buscar todos os utilizadores (da tabela users)
            $users = User::all();

            
            // Percorre todos os utilizadores para buscar o id do utilizador que tem este username (que estamos a visitar o perfil)
            foreach ($users as $user) {
                if ($user->user_name == $username) {
                    $profileId = $user->id;
                }
            }


            // Vamos buscar o registo do utilizador (tabela users) com o id $profileId
            $myself =  User::find($profileId);

        } else { // Se está a visitar o seu próprio perfil
            $myself = Auth::user();
        }

        return $myself;

    }





    /**
     * Este método recebe uma coleção de publicações e devolve apenas as que não são de grupos (faz uma filtragem)
     */
    public function filterDocumentsNotInGroups($documents) {

        $documentsNotInGroups = []; // Documentos que não pertencem a grupos
        $idsOfAllPublicationsInGroups = []; // Guarda todos os ids das publicações feitas em grupos
        $idsOfAllDocumentsInGroups = []; // Guarda todos os ids dos documentos feitos upload em grupos

        $allGroupPublications = GroupPublication::All(); // Vai buscar todas as publicações de grupos


        // Guarda todos os ids das publicações que pertencem a grupos, num array
        foreach($allGroupPublications as $groupPub) {
            $idsOfAllPublicationsInGroups[$groupPub['publication_id']] = $groupPub['publication_id'];
        }

        // Vai buscar todas as publicações dos grupos para ir buscar os documentos delas
        foreach($idsOfAllPublicationsInGroups as $publicationID) {
            $publicationDocuments = Publication::find($publicationID)->documents;

            // Guarda todos os ids dos documentos que pertencem a grupos, num array
            foreach($publicationDocuments as $doc) {
                $idsOfAllDocumentsInGroups[$doc->id] = $doc->id; 
            }
        }

        
        
        // Converter para coleção, para poder utilizar o contains
        $c = collect($idsOfAllDocumentsInGroups);


        // Pecorre todos os documentos enviados como parâmetro (do utilizador)
        foreach ($documents as $doc) {

            if (!$c->contains($doc->id)) { // Se o id desse documento não estiver nos documentos dos grupos, então pode mostrar
                $documentsNotInGroups[$doc->id] = $doc; // Adiciona o documento no array
            }

        }

        return $documentsNotInGroups; // Retorna essas publicações que não estão em grupos

    }




     /**
     * Este método permite procurar um grupo pelo nome
     */
    public function searchUser(request $request) {

        $photoController = new PhotoController(); // Intanciar controlador para usar método de buscar foto de perfil
        $userFriendController = new UserFriendController(); // Instanciar controlador para buscar método findFriendDetails

        $current_user = Auth::User(); // Utilizador logado
        $myself = $current_user;

        $users = []; // Array para guardar todos os utilizadores que encontrar


        if ($current_user) {

            $profile = $photoController->getProfilePhoto($current_user->id); // Foto de perfil do utilizador logado (para preencher a barra do lado direito)

            $search = $request->search; // Input da caixa de texto para procurar utilizador

            $allUsers = User::All(); // Vai buscar todos os utilizadores

            foreach ($allUsers as $user) { // Percorre todos os utilizadores
    
                if (Str::contains($user->name, $search) || Str::contains($user->user_name, $search)) { // Para cada utilizador, verifica se o nome contém o texto da caixa de texto
                    
                    // Se o nome do utilizador contiver o texto da caixa de texto, mostra esse utilizador
                    $users[$user->id] = $userFriendController->findFriendDetails($user); // Adiciona ao array

                }

            }

            $friends = $users;
            
            $option = 2; // Pesquisa de utilizadores (Não é para mostrar apenas amigos)

            return view('test2.friends', compact('current_user', 'friends', 'profile', 'myself', 'option'));

        } else {

            return view('auth.login'); // Se não tiver sessão iniciada, não pode

        }

    }






    
    /**
     * Esta função vai devolver todos os dados para preencher o perfil de um utilizador
     */
    // Caso não seja enviado nenhum argumento, ele coloca como null
    public function getProfile($username = null)
    {

        // Buscar o utilizador atual (que está logado)
        $current_user = Auth::user();


        // Vai buscar o utilizador responsável pelo perfil (cujo username está no url)
        $myself = $this->getUserByUsername($username);
        
        
        if ($myself) {

            // Instanciar controlador para utilizar método de outro controaldor
            $photoController = new PhotoController();

            // Instanciar controlador para utilizar método de outro controlador
            $u = new UserFriendController();
            $friends = $u->findFriends($myself);

            // Buscar fotos do utilizador
            $photos = $myself->photos;

            // Buscar documentos do utilizador
            $documents = $myself->userDocuments;

            // Vai buscar apenas documentos do utilizador que não foram publicados por ele em grupos
            $documents = $this->filterDocumentsNotInGroups($documents);


            // Buscar todas as publicações (no ficheiro profile.blade.php, é restringido com base na variável $myself enviada)
            $p = new PublicationController();
            $publications = $p->getAllPublications();


            // Buscar fotografia de perfil do utilizador logado (para aparecer no menu do lado esquerdo)
            $profile = $photoController->getProfilePhoto($current_user->id);

            // Foto de perfil do utilizador dono do perfil
            $profileOwnerPhoto = $photoController->getProfilePhoto($myself->id);



            // Fazer random dos amigos, fotos e documentos (para não mostrar sempre os mesmos no perfil)
            $friends = collect($friends)->shuffle();
            $documents = collect($documents)->shuffle();
            $photos = collect($photos)->shuffle();
            

            // Verifica se o utilizador do perfil é amigo do utilizador atual
            $isFriend = $u->isFriendOf($myself, $current_user);

            
            
            // Vai buscar o id do pedido de amizade (enviado ou recebido)
            if ($isFriend == 5) { // Pedido de amizade enviado
                $request_id = $u->getRequest($myself, $current_user, 1); // 1 - Enviados
            } else if ($isFriend == 6) { // Pedido de amizade recebido
                $request_id = $u->getRequest($myself, $current_user, 2); // 2 - Recebidos
            } else {
                $request_id = 0;
            }

            return view('test2.profile', compact('current_user', 'myself', 'profile', 'friends', 'photos', 'documents', 'publications', 'isFriend', 'request_id', 'profileOwnerPhoto'));
            
        } else {
            return view('auth.login');
        }
    }





    /**
     * Este método permite editar o perfil do utilizador logado
     */
    public function editProfile(Request $request) {
        
        $current_user = Auth::user(); // Buscar utilizador logado

        // Se o utilizador estiver logado
        if ($current_user) {


            // Um utilizador pode ter vários, por isso são 4 arrays (cada um deles guarda um tipo de dados do formulário)
            $degreesId = $request->degree_id; // Guarda id dos estudos
            $degrees = $request->degree; // Estudo
            $begin = $request->begin; // Começo
            $end = $request->end; // Fim

            
            // Alterar os vários estudos
            for ($i = 0; $i < count($degreesId); $i++) {

                $userDegree = UserDegree::find($degreesId[$i]); // Vai buscar o estudo com o id presente no array
                
                if ($degrees[$i] == "Outro" || $degrees[$i] == "Estudante" || $degrees[$i] == "Licenciado" || $degrees[$i] == "Mestrado" || $degrees[$i] == "Doutorado") {
                    $userDegree->degrees = $degrees[$i]; // Altera o estudo
                }
                
                $userDegree->date_begin = $begin[$i]; // Altera o início
                $userDegree->date_end = $end[$i]; // Altera o fim
                $userDegree->save(); // Guarda as alterações efetuadas

            }


            // $roleID = 1;


            // Efetuar as alterações no utilizador logado
            $current_user->name = $request->name; // Nome
            // $current_user->role_id = $roleID; // Cargo
            $current_user->user_name = $request->username; // Username
            $current_user->phone = $request->phone; // Telemóvel
            $current_user->description = $request->description; // Descrição
             
            $current_user->save();  // Guardar todas as alterações

            return redirect()->back(); // Redireciona para a página anterior
            

        } else {
            return view('auth.login');
        }

    }



    /**
     * Este método mostra toda a informação sobre o utilizador do perfil
     */
    public function getInfo($username = null)
    {
        $photoController = new PhotoController();
        $roleController = new RoleController();

        $current_user = Auth::user();
        
        // Vai buscar o utilizador responsável pelo perfil (cujo username está no url)
        $myself = $this->getUserByUsername($username);

        if ($myself) {
            $profile = $photoController->getProfilePhoto($current_user->id);
            return view('test2.about', compact('current_user', 'myself', 'profile'));
        } else {
            return view('auth.login');
        }
    }


    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id);
        return redirect('/home');
    }
}
