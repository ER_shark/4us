<?php

namespace App\Http\Controllers;

// Modelos usados
use App\User;
use App\Publication;
use App\Comentary;
use App\UserFriend;
use App\Photo;
use App\Document;
use App\Group;
use App\GroupPublication;


use DB;


// API do tempo
use Bioudi\LaravelMetaWeatherApi\Weather;


use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Str; // Para usar o contains das strings (para procurar publicações)


use App\Http\Controllers\PhotoController;
use App\Http\Controllers\GoogleDriveController;


use File; // Facade File, para apagar imagem da pasta public
use Mail; 


class PublicationController extends Controller
{

    /**
     * Function that gets all the data needed to print a publication
     * User, comentary(friends and owner)
     */
    public function getPublication()
    {

        // Buscar temperatura
        $weather = new Weather();
        $day = str_replace("-", "/", Carbon::today()->toDateString()); // Dia atual
        $temperature = (string) $weather->getByCityName('lisbon', $day)[0]->the_temp; // 0 significa a temperatura medida mais recentemente (existem várias medições por dia)

        $mytime = Carbon::now();
        $hour = explode(':', $mytime->toTimeString())[0];

        $msg = "";
        if ($hour >= 19) {
            $msg = "Boa noite";
        } else if ($hour >= 12) {
            $msg = "Boa tarde";
        } else {
            $msg = "Bom dia";
        }
        $msg = $msg . "! Estão " . round($temperature) . "º.";


        $photoController = new PhotoController();
        $current_user = Auth::user();

        if ($current_user) {
            
            $publications = $this->getAllPublications();
            $profile = $photoController->getProfilePhoto($current_user->id);

            return view('test2.test', compact('current_user', 'publications', 'profile', 'msg'));
        } else {
            return view('auth.login');
        }
    }



    /**
     * Este método devolve um array com 3 posições:
     * posição 0 - número de gostos
     * posição 1 - número de adoros
     * posição 2 - número de não gostos
     */
    public function getNumberLikes($likes) {

        $likesArray = [];
        $n_likes = 0;
        $n_loves = 0;
        $n_dislikes = 0;

        foreach($likes as $like) {

            if ($like->like == 0) {
                $n_likes++;
            } else if ($like->like == 1) {
                $n_loves++;
            } else if ($like->like == 2) {
                $n_dislikes++;
            }

        }

        $likesArray[0] = $n_likes;
        $likesArray[1] = $n_loves;
        $likesArray[2] = $n_dislikes;
        
        return $likesArray;

    }





    /**
     * Buscar todos os dados necessários de uma publicação (dados da publicação, documentos, comentários, likes)
     */
    public function getAllPublicationData($allPublications, $publications, $user) {
        

        // Percorrer todas as publicações
        foreach($publications as $publication) {

            
            // Se não for enviado o user que criou a publicação (user = null), vai ter de encontrar o utilizador que criou a publicação
            if ($user == null) {
                $pubUser = User::find($publication->user_id);
            } else {
                $pubUser = $user;
            }

            
            // Guardar dados das publicações
            $allPublications["publication_$publication->id"] = $this->publicationDetails($publication, $pubUser);
                    
            // Buscar comentários de uma publicação
            $commentaries = $publication->comentary;

            // Buscar documentos de uma publicação
            $documents = $publication->documents;


            // Envia os likes da publicação e retorna o número de gostos, o número de adoros e o número de não gostos num array (separa-os)
            $opinions = $this->getNumberLikes($publication->likes);

            // Guardar os gostos, não gostos e adoros de cada publicação
            $allPublications["publication_$publication->id"]['likes'] = $opinions;



            // Percorrer todos os documentos da publicação e guardar
            foreach($documents as $doc) {
                // Guardar dados dos documentos
                $allPublications["publication_$publication->id"]['documents']["document_$doc->id"] = $doc;
            }


            if (count($commentaries) > 0) { // Caso existam comentários


                // Percorrer todos os comentários da publicação e guardar
                foreach($commentaries as $commentary) {

                    // Buscar o utilizador que fez o comentário
                    $userWhoComment = User::find($commentary->user_id); 

                    // Guardar dados dos comentários
                    $allPublications["publication_$publication->id"]['comentaries']["comentarie_$commentary->id"] = $this->comentariesDetails($userWhoComment, $commentary);
                }

                    
                // Ordenar os comentários por ordem crescente da data (com data menor ficam em primeiro)
                // É necessário converter o array para coleção, para ordenar
                $c = collect($allPublications["publication_$publication->id"]['comentaries']);
                // Ordenar por ordem crescente da data do comentário
                $sorted = $c->sortBy('com_date'); 
                // Converter de coleção para array novamente
                $c = $sorted->toArray();


                // Atribuir o array ordenado aos comentários da publicação
                $allPublications["publication_$publication->id"]['comentaries'] = $c;

            }

        }


        // Ordenar as publicações por ordem decrescente da data (com data superior (mais recentes) ficam em primeiro)
        // É necessário converter o array para coleção, para ordenar
        $c = collect($allPublications);
        // Ordenar por ordem decrescente da data da publicação
        $sorted = $c->sortByDesc('pub_date'); 
        // Converter de coleção para array novamente
        $allPublications = $sorted->toArray();


        return $allPublications;

    }




    /**
     * Este método permite buscar as publicações de um determinado utilizador
     */
    public function getUserPublications($userProfileID) {

        // Array onde guardará as informações todas (documentos, comentários, etc) das publicações do utilizador
        $allPublicationsOfUser = [];

        // Instanciar controlador para utilizar os seus métodos
        $userController = new UserController(); 

        // Buscar o utilizador com o id $userProfileID
        $user = User::find($userProfileID);

        // Buscar as publicações do utilizador $user
        $userPublications = $user->userPublications;

        // Vai buscar as publicações dele próprio que não estão em grupos (faz a filtragem)
        $userPublications = $this->filterPublicationsNotInGroups($userPublications);

        // Guarda todas as publicações do utilizador enviado como parâmetro                    
        // Array allPublications é onde irá armazenar (envia por causa de não substituir e apenas acrescentar)
        // userPublications são as publicações que percorrerá para buscar todos os dados necessários
        // user é o dono deas publicações para ir buscar os seus dados (neste caso é sempre o mesmo - depende do argumento enviado)
        $allPublicationsOfUser = $this->getAllPublicationData($allPublicationsOfUser, $userPublications, $user);

        return $allPublicationsOfUser;

    }

    

    /**
     * Este método permite devolver as publicações dos amigos e as suas próprias (QUE NÃO SEJAM DE UM GRUPO)
     */
    public function getAllPublications()
    {

        // Array para guardar todas as publicações
        $allPublications = [];

        // Utilizador logado
        $current_user = Auth::user();


        // Buscar publicações e amigos do utilizador logado
        $currentUserFriends = $current_user->userFriends;
        $currentUserPublications = $current_user->userPublications;

        // Vai buscar as publicações dele próprio que não estão em grupos (faz a filtragem)
        $currentUserPublications = $this->filterPublicationsNotInGroups($currentUserPublications);

        // Guarda todas as publicações do utilizador                        
        // Array allPublications é onde irá armazenar (envia por causa de não substituir e apenas acrescentar)
        // currentUserPublications são as publicações que percorrerá para buscar todos os dados necessários
        // current_user é o dono de cada publicação para ir buscar os seus dados (neste caso é smepre o próprio porque são publicações dele)
        $allPublications = $this->getAllPublicationData($allPublications, $currentUserPublications, $current_user);

        // Percorre todos os amigos dele (para ver as publicações dos amigos dele)
        foreach($currentUserFriends as $friend) {
            
            // user_id é ele próprio e friend_id é o id do amigo
            // Vai buscar as publicações de cada amigo
            $friendPublications = User::find($friend->friend_id)->userPublications;

            // Vai buscar as publicações de cada amigo que não estão em grupos (faz a filtragem)
            $friendPublications = $this->filterPublicationsNotInGroups($friendPublications);

            // utilizador com o id do amigo dele
            $friend = User::find($friend->friend_id); 

            // Envia a lista de publicações $friendPublications e retorna um array com toda a informação sobre cada uma dessas publicações (comentários, likes, documentos, etc)
            $allPublications = $this->getAllPublicationData($allPublications, $friendPublications, $friend);

        }


        return $allPublications;
    }





    /**
     * Este método recebe uma coleção de publicações e devolve apenas as que não são de grupos (faz uma filtragem)
     */
    public function filterPublicationsNotInGroups($publications) {

        $publicationsNotInGroups = []; // Publicações dele ou de amigos, que não estão em grupos
        $idsOfAllPublicationsInGroups = []; // Guarda todos os ids das publicações feitas em grupos

        $allGroupPublications = GroupPublication::All(); // Vai buscar todas as publicações de grupos


        // Guarda todos os ids das publicações que pertencem a grupos, num array
        foreach($allGroupPublications as $groupPub) {
            $idsOfAllPublicationsInGroups[$groupPub['publication_id']] = $groupPub['publication_id'];
        }
        
        // Converter para coleção, para poder utilizar o contains
        $c = collect($idsOfAllPublicationsInGroups);


        // Pecorre todas as publicações enviadas como parâmetro (que pertencem aos amigos ou dele próprio) para verificar quais não estão em grupos
        foreach ($publications as $pub) {

            $public = Publication::find($pub['id']); // Buscar a publicação com este id

            if (!$c->contains($public->id)) { // Se o id desta publicação não estiver nas publicações dos grupos, então pode mostrar
                $publicationsNotInGroups[$public->id] = $public; // Adiciona a publicação no array
            }

        }

        return $publicationsNotInGroups; // Retorna essas publicações que não estão em grupos

    }
    



    /**
     * Preenche o array com detalhes da publicação
     */
    public function comentariesDetails($user, $comentary)
    {

        $photoController = new PhotoController();

        return [
            'com_user_id' => $user->id,
            'com_user_name' => $user->name,
            'com_user_username' => $user->user_name,
            'com_user_photo' => $photoController->getProfilePhoto($user->id),
            'com_id' => $comentary->id,
            'com_description' =>  $comentary->comentary,
            'com_date' =>  $comentary->date
        ];
    }


    /**
     * Preenche o array com detalhes sobre os comentários
     */
    public function publicationDetails($publication, $user)
    {

        $photoController = new PhotoController();

        return [
            'id' => $publication->id,
            'user_id' => $user->id,
            'user_name' => $user->name,
            'user_username' => $user->user_name,
            'user_photo' => $photoController->getProfilePhoto($user->id),
            'pub_description' => $publication->description,
            'pub_date' => $publication->date
        ];
    }

    


    /**
     * Este método permite enviar um email para um membro de um grupo
     */
    public function sendNotificationMail($to_name, $to_email, $teacher_name, $group_name, $pub_date, $pub_description) {
        
        // ENVIAR EMAIL
        $data = array("teacher_name"=> $teacher_name, "group_name" => $group_name, "pub_date" => $pub_date, "publication" => $pub_description);
        Mail::send("test2.mail", $data, function($message) use ($to_name, $to_email) { // Variáveis de fora que serão usadas dentro, são colocadas no use
            $message->to($to_email, $to_name)
            ->subject("Nova Informação - " . Carbon::now());
            $message->from("plataformauniversidade123@gmail.com", "4US - 4UniversityStudents");
        });

    }





    /**
     * Este método permite procurar uma publicação pela descrição (que esteja contido)
     */
    public function searchPublication(request $request) {
        

        // Buscar temperatura
        $weather = new Weather();
        $day = str_replace("-", "/", Carbon::today()->toDateString()); // Dia atual
        $temperature = (string) $weather->getByCityName('lisbon', $day)[0]->the_temp; // 0 significa a temperatura medida mais recentemente (existem várias medições por dia)

        $mytime = Carbon::now();
        $hour = explode(':', $mytime->toTimeString())[0];

        $msg = "";
        if ($hour >= 19) {
            $msg = "Boa noite";
        } else if ($hour >= 12) {
            $msg = "Boa tarde";
        } else {
            $msg = "Bom dia";
        }
        $msg = $msg . "! Estão " . round($temperature) . "º.";


        $publications = [];
        $publicationsFound = []; // Publicações encontradas com a pesquisa

        $photoController = new PhotoController(); // Intanciar controlador para usar método de buscar foto de perfil
        $current_user = Auth::User(); // Utilizador logado

        if ($current_user) {

            $profile = $photoController->getProfilePhoto($current_user->id); // Foto de perfil do utilizador logado (para preencher a barra do lado direito)

            $search = $request->search; // Input da caixa de texto para procurar grupo

            $allPublications = Publication::All(); // Vai buscar todas as publicações

            foreach ($allPublications as $publication) { // Percorre todas as publicações
    
                if (Str::contains($publication->description, $search)) { // Para cada publicação, verifica se a descrição contém o texto da caixa de texto
                    
                    // Se o nome da publicação contiver o texto da caixa de texto, mostra essa publicação
                    $publications[$publication->id] = $publication;

                    // Vai buscar as publicações que não estão em grupos (faz a filtragem)
                    $publications = $this->filterPublicationsNotInGroups(collect($publications));

                    // Guarda todas as publicações encontradas                        
                    // Array publicationsFound é onde irá armazenar (envia no argumento, para não substituir e apenas acrescentar)
                    // publications são as publicações encontradas, que percorrerá para buscar todos os dados necessários (fotos, comentários, documentos, likes, etc)
                    // O último parâmetro a null indica que não sabemos qual é o dono da publicação, mas irá ver quando percorrer cada publicação
                    $publicationsFound = $this->getAllPublicationData($publicationsFound, $publications, null);

                    $publications = $publicationsFound; // Para enviar para o template

                }

            }
            
            return view('test2.test', compact('current_user', 'publications', 'profile', 'msg'));

        } else {

            return view('auth.login'); // Se não tiver sessão iniciada, não pode

        }

    }





    /**
     * adiciona um novo publicaçao
     */
    public function createPublication(Request $request)
    {
        
        $current_user = Auth::user(); // Buscar utilizador que fez login

        if ($current_user) {
            
            // Transação para fazer tudo ou nada
            // Todas as variáveis definidas foram da função, têm de ser colocadas no use, para serem usadas dentro da transação
            DB::transaction(function () use ($request, $current_user) {

                $publication = new Publication;
                $publication->user_id = $current_user->id; // O dono da publicação é o que está logado
                // $publication->category_id = $request->category;
                $publication->category_id = 1;
                $publication->description =  $request->description; // Atribui a descrição do formulário
                // $publication->publication_id = $request->pub_id;
                // $publication->visibility = $request->visibility;
                $publication->visibility = 'public';
                // $publication->accept = $request->accept;
                $publication->accept = 1;
                $publication->date = Carbon::now(); // Data atual
                $publication->save(); // Guarda a publicação





                $documents = $request->file('files'); // Vai buscar os documentos do input (podem ser múltiplos, logo, guarda em array)


                // Cria instância do controlador do google drive, para utilizar método de upload
                $googledrive = new GoogleDriveController();

                // Cria instância do controlador do documento para fazer a inserção de documentos
                $documentController = new DocumentController();


                if ($documents != null) { // Caso seja feito upload de algum documento

                    foreach ($documents as $doc) { // Percorre todos os documentos inseridos, para os guardar

                        $documentController->createDocument($doc, $publication, $current_user, $googledrive);

                    }

                }



                if($request->group_id != null) { // Quer dizer que está a inserir num grupo e não no perfil

                    // Logo, é necessário adicionar a publicação ao grupo
                    $publicationGroup = new GroupPublication;
                    $publicationGroup->publication_id = $publication->id; // ID da publicação, inserido anteriormente
                    $publicationGroup->group_id = $request->group_id; // ID do grupo, vindo do formulário hidden
                    $publicationGroup->save(); // Guarda as alterações na base de dados


                    // Buscar nome do grupo
                    $group_name = Group::find($request->group_id)->name;


                    // Vai percorrer todos os membros do grupo para enviar email
                    $groupUsers = Group::find($request->group_id)->userGroups;


                    foreach($groupUsers as $group) {

                        if ($group->roles == "member") { // Só envia mensagem para os membros (não envia para os professores nem dono do grupo)

                            $user = User::find($group->user_id); // Vai buscar o utilizador membro

                            // Enviar email de notificação para cada membro
                            $this->sendNotificationMail($user->name, $user->email, $current_user->name, $group_name, $publication->date, $publication->description);

                        }

                    }

                }

            });

            // Redireciona para a página 
            if ($request->current_url == "/publication/search") { // Se estiver na pesquisa, vai para a página inicial
                return redirect('/home');
            } else {
                return redirect()->back(); // Redireciona de volta
            }

        } else {
            return redirect('/');
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Publication  $publication
     * @return \Illuminate\Http\Response
     */
    public function show(Publication $publication)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Publication  $publication
     * @return \Illuminate\Http\Response
     */
    public function edit(Publication $publication)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Publication  $publication
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Publication $publication)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Publication  $publication
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $current_url)
    {
        $current_user = Auth::user();
        if ($current_user) {
            Publication::destroy($id);
            
            // Redireciona para a página 
            if ($current_url == 0) { // Se estiver na pesquisa, vai para a página inicial
                return redirect('/home');
            } else {
                return redirect()->back(); // Redireciona de volta
            }
        
        } else {
            return redirect('/login');
        }
    }
}
