<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;


use App\Http\Controllers\PublicationController;
use App\Http\Controllers\GroupController;



use App\PublicationLike;
use App\Publication;


class AjaxController extends Controller
{
    // Buscar 5 posts para atualizar a página
    public function getPosts(request $request) {

        $current_user = Auth::User(); // Utilizador logado

        // OBRIGATÓRIOS PARA TODOS
        $start = $request->start; // Vai buscar o start enviado no ajax
        $option = $request->option; // Vai buscar a opção enviada no ajax, para saber se devolve as publicações dele e dos amigos, só do perfil ou de um grupo

        // DEPENDEM DA OPÇÃO
        $groupID = $request->groupID; // ID do grupo (caso haja)
        $userProfileID = $request->userProfileID; // ID do utilizador dono do perfil


        // Instanciar controladores para buscar publicações
        $p = new PublicationController(); 
        $g = new GroupController();

        if ($option == 1) {
            $publicationsList = $p->getAllPublications(); // Buscar todas as publicações dele ou de amigos
        } else if ($option == 2) {
            $publicationsList = $p->getUserPublications($userProfileID); // Buscar publicações do dono do perfil
        } else if ($option == 3) {
            $publicationsList = $g->getGroupPublications($groupID, $current_user); // Buscar todas as publicações de um grupo
        }


        $num_shown = count($publicationsList) - $start; // Número de publicações total - o número das que já foram mostradas, indica quantas falta mostrar na página
        
        // Se o número das publicações que falta mostrar forem maior que 5, apenas mostrará 5, senão tem de mostrar esse número (todas as que faltam, por não excederem 5)
        if ($num_shown > 5) {
            $num_shown = 5;
        }
                
        $collection = collect($publicationsList); // Transforma em coleção para utilizar os métodos
        $collection = $collection->splice($start, $num_shown); // Mostra num_shown publicações a partir do start

        $publicationsList = $collection->toArray(); // Converte a coleção novamente para array

        $last = $start + $num_shown; // O último que mostrou é o que começou mais o número de quantas publicações mostrou
        
        return response()->json(array('publications'=> $publicationsList, 'last' => $last), 200);
    }

    


    /**
     * Recebe uma data e devolve a diferença para humanos (meses, dias, anos, etc)
     */
    public function getDiffForHumans(request $request) {

        $date = $request->date;

        Carbon::now()->setlocale('pt');
        $diffForHumans = Carbon::now()->diffForHumans(Carbon::parse($date));

        return $diffForHumans; // Retorna a diferença para humanos

    }

     



    /**
     * Atribuir reação a uma publicação (gosto, adoro ou não gosto)
     */
    public function addLike(request $request) {
        
        // Vai buscar os parâmetros enviados pelo ajax
        $val = $request->val; // tipo de reação (0 - gosto, 1 - adoro, 2 - não gosto)
        $pub = $request->pub; // id da publicação

        $current_user = Auth::user(); // Vai buscar o utilizador logado (foi o que reagiu)


        $likeAlreadyExists = false;


        if ($current_user) {


            // Apaga reação anteriores deste utilizador nesta publicação (caso haja) (só pode existir uma)
            $likes = $current_user->likes;


            // Percorre todas as reações do utilizador à procura de anterior reação a esta publicação
            // Caso haja uma reação deste utilizador nesta publicação, em vez de criar, altera a reação
            foreach ($likes as $like) {

                if ($like->publication_id == $pub) {


                    if ($like->like == $val) { // Se já existia esta reação, apaga-a

                        $like->forceDelete(); // Apaga a reação existente
                        $likeAlreadyExists = true;

                    } else { // Se a reação que tinha anteriormente é diferente, substitui

                        $like->like = $val;
                        $like->save();

                        $likeAlreadyExists = true;

                    }

                }

            }

            // Se ainda não existe reação deste utilizador nesta publicação, cria
            if (!$likeAlreadyExists) {

                // Adiciona a nova reação
                $like = new PublicationLike; // Cria uma nova reação
                $like->like = $val;
                $like->publication_id = $pub;
                $like->user_id = $current_user->id;
                $like->save(); // Guarda na base de dados

            }


        }


        // Após fazer a reação, vai devolver as reações existentes (para atualizar)
        $publicationController = new PublicationController();

        $publication = Publication::find($pub);

        // Envia os likes da publicação como argumento, e retorna o número de gostos, o número de adoros e o número de não gostos num array (separa-os)
        $opinions = $publicationController->getNumberLikes($publication->likes);

        // Devolve as reações
        return $opinions;


    }



}
