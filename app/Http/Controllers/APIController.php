<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;


// Modelos a usar
use App\User;
use App\Publication;
use App\Document;
use App\Photo;
use App\Comentary;
use App\Token;


use Mail; // Para enviar email



class APIController extends Controller
{


    /**
     * Esta função permite retornar um token para o utilizador logado
     */
    public function createToken() {

        $userToken = ""; // Devolver o token criado

        if ($this->getActiveToken() == false) { // Não tem token ativo (logo cria novo)

            $token = new Token; // Cria token
            $token->user_id = Auth::User()->id; // Cria token para o utilizador logado
            $token->token = str_random(32); // Cria token random de 32 carateres
            $token->validation_date = Carbon::now()->addHour(); // O token é válido por 1 hora
            $token->save(); // Guarda a mudança

            $userToken = $token->token;
            $validation_date = $token->validation_date;

        } else { // Tem token ativo (logo não cria novo)

            $userToken = $this->getActiveToken()->token;

            $validation_date = $this->getActiveToken()->validation_date;

        }


        $user = Auth::User(); // Utilizar logado


        // ENVIAR EMAIL A AVISAR DO TOKEN
        $to_name = $user->name;
        $to_email = $user->email;

        $data = array("user"=> $user->name, "token" => $userToken, "validation_date" => $validation_date);
        Mail::send("test2.token_mail", $data, function($message) use ($to_name, $to_email) { // Variáveis de fora que serão usadas dentro, são colocadas no use
            $message->to($to_email, $to_name)
            ->subject("Pedido de Token");
            $message->from("plataformauniversidade123@gmail.com", "4US - 4UniversityStudents");
        });
        
        return redirect()->back(); // Redireciona para trás

    }



    /**
     * Esta função devolve se um utilizador já tem um token ativo ou não
     */
    public function getActiveToken() {

        $userId = Auth::User()->id; // Id do utilizador logado

        $allTokens = Token::All(); // Todos os tokens

        $tokenReturned = false;

        // Percorre todos os tokens
        foreach($allTokens as $token) {
            if ($token->user_id == $userId && $token->validation_date >= Carbon::now()) {
                $tokenReturned = $token;
            }
        }

        return $tokenReturned; // Retorna o token ou false (se tem token ativo ou não)

    }




    /**
     * Esta função retorna o token do utilizador logado, caso exista
     */
    public function compareToken($receivedToken) {

        $userId = Auth::User()->id; // Id do utilizador logado

        $allTokens = Token::All(); // Todos os tokens

        $token = "";

        //  Percorre todos os tokens
        foreach($allTokens as $token) {
            if ($token->user_id == $userId && $token->validation_date >= Carbon::now()) {
                $token = $token->token;
            }
        }

        return $receivedToken == $token;

    }



    /* Buscar todas as publicações feitas por um determinado utilizador */
    public function getPublicationsOfUser($userID, $token) {

        if ($this->compareToken($token)) { // Caso o token esteja correto e não tenha expirado

            $userPublications = User::find($userID)->userPublications; // Vai buscar as publicações do utilizador
            return response()->json($userPublications); // Devolve em json

        } else {

            return response()->json("Não tem permissões para aceder ou o token expirou.");

        }

        
    }


    /* Buscar todos os comentários de uma determinada publicação */
    public function getCommentsOfPublication($publicationID, $token) {

        if ($this->compareToken($token)) { // Caso o token esteja correto e não tenha expirado

            $publicationComments = Publication::find($publicationID)->comentary; // Vai buscar comentários da publicação
            return response()->json($publicationComments); // Devolve em json

        } else {

            return response()->json("Não tem permissões para aceder ou o token expirou.");

        }

    }


    /* Buscar todos os utilizadores existentes */
    public function getAllUsers($token) {

        if ($this->compareToken($token)) { // Caso o token esteja correto e não tenha expirado

            $users = User::all(); // Vai buscar utilizadores
            return response()->json($users); // Devolve em json

        } else {

            return response()->json("Não tem permissões para aceder ou o token expirou.");

        }

    }

    
    /* Buscar todas as publicações existentes */
    public function getAllPublications($token) {

        if ($this->compareToken($token)) { // Caso o token esteja correto e não tenha expirado

            $publications = Publication::all(); // Vai buscar publicações
            return response()->json($publications); // Devolve em json

        } else {

            return response()->json("Não tem permissões para aceder ou o token expirou.");

        }

    }


    /* Buscar amigos de um determinado utilizador */
    public function getUserFriends($userID, $token) {

        if ($this->compareToken($token)) { // Caso o token esteja correto e não tenha expirado

            $userPublications = User::find($userID)->userFriends; // Vai buscar amigos de um utilizador
            return response()->json($userPublications); // Devolve em json

        } else {

            return response()->json("Não tem permissões para aceder ou o token expirou.");

        }

    }



    /* Buscar documentos de uma determinada publicação */
    public function getDocumentsOfPublication($publicationID, $token) {

        if ($this->compareToken($token)) { // Caso o token esteja correto e não tenha expirado

            $publicationDocuments = Publication::find($publicationID)->documents; // Vai buscar documentos de uma publicação
            return response()->json($publicationDocuments); // Devolve em json

        } else {

            return response()->json("Não tem permissões para aceder ou o token expirou.");

        }

    }


    /* Buscar todos os comentários de um determinado utilizador */
    public function getCommentsOfUser($userID, $token) {

        if ($this->compareToken($token)) { // Caso o token esteja correto e não tenha expirado

            $userCommentaries = User::find($userID)->comentaries; // Vai buscar comentários feitos por um utilizador
            return response()->json($userCommentaries); // Devolve em json

        } else {

            return response()->json("Não tem permissões para aceder ou o token expirou.");

        }

    }


    /* Buscar todos os documentos de um determinado utilizador */
    public function getDocumentsOfUser($userID, $token) {

        if ($this->compareToken($token)) { // Caso o token esteja correto e não tenha expirado

            $userDocuments = User::find($userID)->userDocuments; // Vai buscar documentos de um utilizador
            return response()->json($userDocuments); // Devolve em json

        } else {

            return response()->json("Não tem permissões para aceder ou o token expirou.");

        }

    }


    /* Buscar todos os documentos existentes */
    public function getAllDocuments($token) {

        if ($this->compareToken($token)) { // Caso o token esteja correto e não tenha expirado

            $documents = Document::all(); // Vai buscar todos os documentos
            return response()->json($documents); // Devolve em json

        } else {

            return response()->json("Não tem permissões para aceder ou o token expirou.");

        }

    }


    /* Buscar todos os comentários existentes */
    public function getAllComments($token) {

        if ($this->compareToken($token)) { // Caso o token esteja correto e não tenha expirado

            $comments = Comentary::all(); // Vai buscar todos os comentários feitos em publicações
            return response()->json($comments); // Devolve em json

        } else {

            return response()->json("Não tem permissões para aceder ou o token expirou.");

        }

    }



    /* Buscar todas as fotografias de um determinado utilizador */
    public function getPhotosOfUser($userID, $token) {

        if ($this->compareToken($token)) { // Caso o token esteja correto e não tenha expirado

            $userPhotos = User::find($userID)->photos; // Vai buscar todas as fotos de um utilizador
            return response()->json($userPhotos); // Devolve em json

        } else {

            return response()->json("Não tem permissões para aceder ou o token expirou.");

        }

    }


    /* Buscar todas as fotografias existentes */
    public function getAllPhotos($token) {

        if ($this->compareToken($token)) { // Caso o token esteja correto e não tenha expirado

            $photos = Photo::all(); // Vai buscar todas as fotos
            return response()->json($photos); // Devolve em json

        } else {

            return response()->json("Não tem permissões para aceder ou o token expirou.");

        }

    }


    
}

