<?php

namespace App\Http\Controllers;

// Modelos que são usados
use App\Group;
use App\GroupUser;
use App\User;
use App\Publication;
use App\GroupPublication;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;



use DB; // Para utilizar transações


// Controladores que são usados
use App\Http\Controllers\PhotoController;
use App\Http\Controllers\GroupPublicationController;
use App\Http\Controllers\PublicationController;
use App\Http\Controllers\UserFriendController;


class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }




    /**
     * Este método permite buscar o dono de um grupo, cujo id foi enviado por parâmetro
     */
    public function getOwnerOfGroup ($groupID) {

        $usersInGroup = GroupUser::all(); // Vai buscar todos os utilizadores que estão em grupos
        $owner = null;

        // Percorre todos os utilizadores em grupos
        foreach($usersInGroup as $user) {

            // Se o utilizador estiver no grupo enviado e for dono
            if ($user->group_id == $groupID and $user->roles == "owner") {
                $owner = $user;
            }

        }

        return $owner; // Retorna o dono do grupo

    }







    /**
     * Este método permite editar os dados do grupo (apenas o dono do grupo pode)
     */
    public function editGroup(Request $request) {
        
        $current_user = Auth::user(); // Buscar utilizador logado

        // Se o utilizador estiver logado
        if ($current_user) {

            // Buscar id do grupo que foi escondido no formulário
            $group_id = $request->group_id;

            // Buscar grupo com o id $group_id
            $group = Group::find($group_id);


            // Efetuar as alterações no grupo
            $group->name = $request->name; // Nome
            $group->description = $request->description; // Descrição
            $group->save();  // Guardar todas as alterações

            return redirect()->back(); // Redireciona para a página anterior
            

        } else {
            return view('auth.login');
        }

    }








    /**
     * Este método mostra a informação sobre o grupo
     */
    public function showInfo($groupID)
    {

        // Instancia controlador para utilizar métodos de outros controladores
        $photoController = new PhotoController();

        $current_user = Auth::user(); // Utilizador logado

        if ($current_user) { // Se estiver logado

            // Vai buscar o grupo com o id groupID
            $group = Group::find($groupID); 
            
            // Vai buscar o dono do grupo
            $ownerID = $this->getOwnerOfGroup($groupID)->user_id;
        
            // Buscar foto do utilizador logado (para mostrar na barra do lado esquerdo)
            $profile = $photoController->getProfilePhoto($current_user->id);

            // Vai para a view
            return view('test2.group_about', compact('current_user', 'profile', 'group', 'ownerID'));

        } else { // Se não tiver sessão iniciada
            return view('auth.login');
        }
    }




    /**
     * Este método permite buscar os membros de um grupo
     */
    public function getMembersOfGroup($groupID) {

        // Instancia controlador para utilizar métodos de outros controladores
        $photoController = new PhotoController();

        $userGroups = Group::find($groupID)->userGroups; // Vai buscar utilizadores do grupo com o id $groupID

        $groupMembers = []; // Array para guardar todos os membros do grupo
        // $num_members = count($group);

        foreach($userGroups as $member) {
            $user = User::find($member->user_id); // Buscar membro através do seu id
            $groupMembers[$member->user_id] = $user; // Guardar membro num array
            $groupMembers[$member->user_id]['role'] = $member->roles; // Guardar papel do utilizador do grupo
            $groupMembers[$member->user_id]['photo'] = $photoController->getProfilePhoto($member->user_id); // Guardar fotografia do utilizador do grupo

        }
        
        return $groupMembers;

    }



    /**
     * Este método mostra os membros do grupo
     */
    public function showMembers($groupID)
    {

        // Instancia controlador par air buscar fotografia de perfil do utilizador logado
        $photoController = new PhotoController();

        $current_user = Auth::user(); // Vai buscar utilizador logado

        if ($current_user) { // Se o utilizador estiver logado

            // Vai buscar membros do grupo
            $groupMembers = $this->getMembersOfGroup($groupID);

            // Vai buscar o dono do grupo
            $ownerID = $this->getOwnerOfGroup($groupID)->user_id;

            // Vai buscar fotografia para o menu do lado esquerdo
            $profile = $photoController->getProfilePhoto($current_user->id);

            return view('test2.group_members', compact('current_user', 'ownerID', 'groupID', 'groupMembers', 'profile'));
       
        } else {
            return view('auth.login');
        }
    }




     /**
     * Este método permite verificar se um utilizador tem permissões para adicionar publicações e apagá-las, ou apagar comentários
     */
    public function currentUserHasPermissions($current_user, $groupID) {

        // Vai buscar os utilizadores de um grupo com o id $groupID
        $usersOfTheGroup = Group::find($groupID)->userGroups;

        $userHasPermissions = false;

        // Percorre todos os utilizadores do grupo para ver se o utilizador logado é professor ou dono do grupo
        foreach($usersOfTheGroup as $user) {
            if (($user->user_id == $current_user->id) && ($user->roles == "owner" || $user->roles == "professor")) {
                $userHasPermissions = true; // Se estiver lá, devolve true
            }
        }

        return $userHasPermissions;

    }




    /**
     * Este método devolve os documentos de um determinado grupo
     */
    public function getGroupDocuments ($groupID) {

        $documentsOfGroup = []; // Array para guardar os docuemntos do grupo

        // Vai buscar todas as publicações deste grupo
        $groupPublications = Group::find($groupID)->groupPublications;


         // Percorre todos os registos da tabela group_publication deste grupo
         foreach($groupPublications as $groupPub) {

                // Vai buscar cada publicação associada ao publication_id (com group_id igual ao deste grupo)
                $publication = Publication::find($groupPub->publication_id);

                // Vai buscar os documentos dessa publicação
                $publicationDocuments = $publication->documents;

                foreach($publicationDocuments as $document) {
                    $documentsOfGroup[$document->id] = $document;
                }

        }

        return $documentsOfGroup; // Retorna os documentos deste grupo

    }




    /**
     * Este método mostra os documentos do grupo
     */
    public function showDocuments ($groupID) {

        $current_user = Auth::user(); // Utilizar logado

        // Instanciar controlador para utilizar método de outro controlador
        $photoController = new PhotoController();


        if ($current_user) { // Se utilizador está logado

            // Buscar documentos do utilizador $myself (dono do perfil)
            $documents = $this->getGroupDocuments($groupID);

            // Buscar fotografia de perfil do utilizador com id $myself->id
            $profile = $photoController->getProfilePhoto($current_user->id);

            return view('test2.group_documents', compact('current_user', 'profile', 'documents'));

        } else { // Caso o utilizador não esteja logado, não pode fazer a operação
            return view('auth.login');
        }

    }





    /**
     * Este método permite mostrar os grupos que um utilizador se encontra e sugestão de 10 grupos random
     */
    public function showGroups($username = null) {

        $current_user = Auth::User(); // Utilizador logado


        // Instanciar controlador para utilizar método de outro controlador
        $userController = new UserController();
        $userFriendController = new UserFriendController();


        // Vai buscar o utilizador responsável pelo perfil (cujo username está no url)
        $myself = $userController->getUserByUsername($username);


        $userGroupsArray = []; // Guarda todos os grupos de um utilizador

        $photoController = new PhotoController(); // Intanciar controlador para usar método de buscar foto de perfil

        if ($current_user) {


            // Verifica se o utilizador do perfil é amigo do utilizador atual
            $isFriend = $userFriendController->isFriendOf($myself, $current_user);


            if ($isFriend == 1 || $myself->id == $current_user->id) { // Se for amigo dele ou for ele próprio, pode ver os amigos

                $userGroups = $this->getUserGroups($myself); // Buscar grupos do dono do perfil (que pode ser o próprio utilizador)
                
                foreach ($userGroups as $group) {
                    $groupObject = Group::find($group->group_id); // Procura o grupo com esse id
                    $userGroupsArray[$group->id] = $groupObject; // Guarda o grupo
                }

                // Vai ser enviado para o template
                $userGroups = $userGroupsArray;

                
                $suggestions = $this->getGroupSuggestions($myself); // Buscar sugestões (grupos em que o utilizador não está inscrito)

                $profile = $photoController->getProfilePhoto($current_user->id); // Foto de perfil do utilizador logado (para preencher a barra do lado direito)

            
                return view('test2.groups', compact('current_user', 'profile', 'myself', 'userGroups', 'suggestions')); // Vai para esta view e envia as variáveis

            } else {
                // Se não for amigo, volta para a página anterior
                return redirect()->back();
            }


        } else {
            return view('auth.login');
        }

    }
    




    /**
     * Este método permite verificar se um utilizador está inserido num grupo
     */
    public function currentUserIsAtGroup($current_user, $groupID) {

        // Vai buscar os utilizadores de um grupo com o id $groupID
        $usersOfTheGroup = Group::find($groupID)->userGroups;

        $userIsMember = false;

        // Percorre todos os utilizadores do grupo para ver se o utilizador logado está lá
        foreach($usersOfTheGroup as $user) {
            if ($user->user_id == $current_user->id) {
                $userIsMember = true; // Se estiver lá, devolve true
            }
        }

        return $userIsMember;

    }





    /**
     * Este método retorna todas as publicações de um grupo
     */
    public function getGroupPublications($groupID, $current_user) {

        $publicationsOfGroup = [];

        $groupPublicationController = new GroupPublicationController(); // Controlador para ir buscar publicações do grupo

        // Buscar publicações do grupo (devolve da tabela GroupPublication)
        $groupPublications = $groupPublicationController->getGroupPublications($groupID);

        // Buscar todos os dados sobre as publicações do grupo (comentários, likes, documentos, etc)
        $publicationsOfGroup = $this->getAllGroupPublicationData($publicationsOfGroup, $groupPublications);

        return $publicationsOfGroup;

    }




    /**
     * Esta função permite aceder a um grupo em que já somos membros
     */
    public function accessGroup($groupID) {
        
        // Array para guardar todas as publicações do grupo
        $publicationsOfGroup = [];

        $current_user = Auth::User(); // Utilizador logado
        $photoController = new PhotoController(); // Intanciar controlador para usar método de buscar foto de perfil

        if ($current_user) { // Se é membro do grupo



            // Devolve se o utilizador atual está inscrito num grupo ou não
            $userIsMember = $this->currentUserIsAtGroup($current_user, $groupID);

            // ADICIONAR AUTOMATICAMENTE AO GRUPO OCORRÊNCIAS UNIVERSIDADE
            // Se o id do grupo for 1, quer dizer que é o grupo onde são publicadas as ocorrências (Ocorrências Universidade)
            if ($groupID == 1 && $userIsMember == false) {

                $groupUser = new GroupUser;
                $groupUser->user_id = $current_user->id; // Id do utilizador logado
                $groupUser->group_id = $groupID; // Grupo Ocorrências Universidade
                $groupUser->roles = "member"; // Entra como membro
                $groupUser->save(); // Guarda alteração

            }



            // Devolve se o utilizador atual está inscrito num grupo ou não
            $userIsMember = $this->currentUserIsAtGroup($current_user, $groupID);

            

            // Verificar se o utilizador logado é realmente membro (se tem permissões)
            if ($userIsMember) {

                $profile = $photoController->getProfilePhoto($current_user->id); // Foto de perfil do utilizador logado (para preencher a barra do lado direito)
                
                // Vai buscar as publicações do grupo
                $publicationsOfGroup = $this->getGroupPublications($groupID, $current_user);

                 // Vai buscar o dono do grupo
                $ownerID = $this->getOwnerOfGroup($groupID)->user_id;

                // Indica se o utilizador atual tem permissões para adicionar e apagar publicações, e apagar comentários (se é professor ou dono devolve true)
                $userHasPermissions = $this->currentUserHasPermissions($current_user, $groupID);
                

                // Buscar os documentos do grupo
                $documents = $this->getGroupDocuments($groupID);

                // Buscar os membros do grupo
                $members = $this->getMembersOfGroup($groupID);


                // Fazer random dos membros e documentos (para não mostrar sempre os mesmos no perfil)
                $members = collect($members)->shuffle();
                $documents = collect($documents)->shuffle();


                return view('test2.group_profile', compact('current_user', 'profile', 'publicationsOfGroup', 'groupID', 'ownerID', 'userHasPermissions', 'documents', 'members')); // Vai para esta view e envia as variáveis

            } else { // Se não é membro do grupo, não pode aceder
                return redirect('/groups'); // Volta para a página anterior
            }

            

        }


    }




    /**
     * Este método permite procurar um grupo pelo nome
     */
    public function searchGroup(request $request) {


        $userGroups = []; // Grupos procurados em que o utilizador já se encontra
        $suggestions = []; // Grupos procurados em que o utilizador ainda não se encontra

        $photoController = new PhotoController(); // Intanciar controlador para usar método de buscar foto de perfil
        $current_user = Auth::User(); // Utilizador logado
        $myself = $current_user;

        if ($current_user) {

            $profile = $photoController->getProfilePhoto($current_user->id); // Foto de perfil do utilizador logado (para preencher a barra do lado direito)

            $search = $request->search; // Input da caixa de texto para procurar grupo

            $allGroups = Group::All(); // Vai buscar todos os grupos

            foreach ($allGroups as $group) { // Percorre todos os grupos
    
                if (Str::contains($group->name, $search)) { // Para cada grupo, verifica se o nome contém o texto da caixa de texto
                    
                    // Se o nome do grupo contiver o texto da caixa de texto, mostra esse grupo

                    // Verifica se já é membro ou ainda não, para mostrar de diferente forma (entrar no grupo ou ver grupo)
                    $userIsMember = $this->currentUserIsAtGroup($current_user, $group->id);
                    if ($userIsMember) { // Se o utilizador já está inscrito no grupo procurado
                        $userGroups[$group->id] = $group;
                    } else { // Se o utilizador ainda não está inscrito no grupo procurado
                        $suggestions[$group->id] = $group;
                    }


                }

            }
            

            return view('test2.groups', compact('current_user', 'profile', 'myself', 'userGroups', 'suggestions')); // Vai para esta view e envia as variáveis

        } else {

            return view('auth.login'); // Se não tiver sessão iniciada, não pode

        }

    }





    /**
     * Este método permite criar um novo grupo (apenas por professores, administradores ou funcinoários)
     */
    public function createGroup(request $request) {


        // Não pode existir um grupo sem dono, logo, quando um grupo é criado, quem criou o grupo tem de ser o dono
        // Caso não seja inserido um dono, não pode existir um grupo, o mesmo o oposto (se não existir grupo, não pode existir dono)
        // Desta forma, utilizamos transações que garantem que, ou efetua as 2 coisas, ou não efetua nenhuma (ou seja, ou cria o grupo e atribui o dono, ou não faz nenhum)
        $gr = DB::transaction(function () use ($request) {

            $group = new Group; // Cria grupo
            $group->name  = $request->name; // Atribui nome vindo do formulário
            $group->description = $request->description; // Atribui descrição vinda do formulário
            $group->save(); // Guarda na base de dados (efetua as alterações)
            
            // ENTRA NO GRUPO
            // Inscreve-se como owner do grupo
            $this->joinGroup($group->id, "owner");

            return $group;

        });

        // Vai para a rota com o name accessGroup e envia essa variável (vai para o grupo criado)
        return redirect()->route('accessGroup', ['groupID' => $gr->id]); 

    }





    /**
     * Este método permite buscar os dados (comentários, likes, documentos, etc) das publicações do grupo
     */
    public function getAllGroupPublicationData($publicationsOfGroup, $groupPublications) {

        $publicationController = new PublicationController(); // Controlador para ir buscar dados das publicações do grupo

        // Guarda todos os dados das publicações (likes, comentários, documentos, etc)
        $listOfPublicationsOfGroup = [];

        // Percorrer todos as publicações do grupo (possuem id do grupo e id da publicação) para buscar apenas as publicações associadas ao id publication_id
        // Isto percorre a tabela group_publications e não a tabela publication
        foreach($groupPublications as $groupPub) {

            $pub = Publication::find($groupPub->publication_id); // Vai buscar a publicação com esse id
            $listOfPublicationsOfGroup[$groupPub->publication_id] = $pub; // Guarda a publicação no array

        }


        // Percorrer cada group_id da tabela GroupPublication para ir buscar dados da publicação
        // Foreach é feito dentro do método
        // Array publicationsOfGroup é onde irá armazenar (envia por causa de não substituir e apenas acrescentar)
        // groupPublications são as publicações que percorrerá para buscar todos os dados necessários
        // user é o dono de cada publicação para ir buscar os seus dados (neste caso é smepre o próprio porque são publicações dele)
        $allPublications = $publicationController->getAllPublicationData($publicationsOfGroup, $listOfPublicationsOfGroup, null); // É enviado null porque não percorremos o foreach, não sabemos qual é o utilizador que criou a publicação antemão, logo, irá identificar quando percorrer
    

        return $allPublications;

    }




    /**
     * Esta função permite inscrever num grupo em que não somos membros ainda.
     * Caso não seja especificado papel, é membro
     */
    public function joinGroup($groupID, $paper = "member") {

        // Buscar utilizador logado
        $current_user = Auth::User();

        $groupUser = new GroupUser;
        $groupUser->user_id = $current_user->id; // Utilizador logado
        $groupUser->group_id = $groupID; // Grupo que pretende entrar
        $groupUser->roles = $paper; // Quem entra num grupo e não foi o próprio a criar, fica membro
        $groupUser->save(); // Guardar na base de dados

        // Vai para a rota com o name accessGroup e envia essa variável
        return redirect()->route('accessGroup', ['groupID' => $groupID]); 
        
    }








    /**
     * Esta função permite sair de um grupo em que somos membros.
     */
    public function leaveGroup($groupID) {

        // Buscar utilizador logado
        $current_user = Auth::User();

        $groupUsers = GroupUser::All(); // Vai buscar todos os utilizadores em grupos

        foreach($groupUsers as $group) { // Vai ver todos os utilizadores em grupos

            // Se este utilizador estiver neste grupo, apaga o registo (tira-o do grupo)
            if ($group->user_id == $current_user->id && $group->group_id == $groupID) {
                $group->forceDelete(); 
            }

        }

        // Vai para a rota com o name groupsList
        return redirect()->route('groupsList'); 
        
    }



    


    /**
     * Esta função permite que o dono de um grupo apague o grupo.
     */
    public function deleteGroup($groupID) {

        $group = Group::find($groupID); // Procura o grupo com o id $groupID

        $group->forceDelete(); // Apaga o grupo
      
        // Vai para a rota com o name groupsList
        return redirect()->route('groupsList'); 
        
    }




    /**
     * Esta função permite que o dono do grupo apague alguém do grupo.
     */
    public function removeMember($groupID, $memberID) {

        $groupUsers = GroupUser::All(); // Vai buscar todos os utilizadores em grupos

        foreach($groupUsers as $group) { // Vai ver todos os utilizadores em grupos

            // Se o membro escolhido estiver neste grupo, apaga o registo (tira-o do grupo)
            if ($group->user_id == $memberID && $group->group_id == $groupID) {
                $group->forceDelete(); 
            }

        }

        // Volta para a página anterior
        return redirect()->back();
        
    }





    /**
     * Este método devolve todos os grupos em que o utilizador logado não está inscrito (sugestões)
     */
    public function getGroupSuggestions($current_user) {

        $idsOfUserGroups = []; // Guarda ids dos grupos em que o utilizador está inscrito
        $suggestions = []; // Guarda grupos que o utilizador não está inscrito
        
        $userGroups = $this->getUserGroups($current_user); // Vai buscar todos os grupos em que o utilizador está inscrito


        // Guarda todos os ids dos grupos em que o utilizador está inscrito, num array
        foreach($userGroups as $group) {
            $idsOfUserGroups[$group['group_id']] = $group['group_id'];
        }
        // Converter para coleção, para poder utilizar o contains
        $c = collect($idsOfUserGroups);


        $allGroups = Group::all(); // Vai buscar todos os grupos existentes


        // Pecorre todos os grupos para obter os grupos em que o utilizador não está inscrito
        foreach ($allGroups as $group) {

            if (!$c->contains($group->id)) { // Se o id deste grupo não estiver nos grupos do utilizador
                $suggestions[$group->id] = $group; // Adiciona o grupo ao array
            }

        }


        // Devolver apenas 5 após fazer random
        $collection = collect($suggestions); // Converter de array para coleção (para usar métodos)
        $collection = $collection->shuffle(); // Baralha o array (random) - para devolver sugestões random e não sempre as mesmas
        $collection = $collection->slice(0, 6); // Mostra 6 a começar na posição 0 (no máximo)
        $suggestions = $collection->toArray(); // Converte de coleção para array (de volta)
        
        return $suggestions; // Retorna lista de grupos em que o utilizador não está inscrito (são sugestões de grupos)

    }




    /**
     * Este método permite atribuir o papel de professor a um membro do grupo (apenas o dono do grupo pode)
     */
    public function makeMemberOrProfessor($groupID, $memberID, $option) {

        $groupUsers = GroupUser::All(); // Vai buscar todos os utilizadores em grupos

        foreach($groupUsers as $group) { // Vai ver todos os utilizadores em grupos

            // Se o membro escolhido estiver neste grupo, torna-o professor
            if ($group->user_id == $memberID && $group->group_id == $groupID) {

                if ($option == 1) {
                    $group->roles = "professor"; // Torna professor
                } else if ($option == 2) {
                    $group->roles = "member"; // Torna membro
                }
                $group->save(); // Guarda a alteração
            }

        }

        // Volta para a página anterior
        return redirect()->back();
        
    }
 




    /**
     * Este método devolve os grupos em que o utilizador logado está inscrito
     */
    public function getUserGroups($current_user) {

        // Para armazenar todos os dados dos grupos
        $userGroups = [];


        $groups = $current_user->userGroups; // Grupos do utilizador logado

        foreach ($groups as $group) {

            $userData = User::find($group->user_id); // Vai buscar o utilizador com o id user_id
            $groupData = Group::find($group->group_id); // Vai buscar o grupo com o id group_id

            $userGroups["group_" . $group->id] = $group; // Guarda o registo GroupUser (Papel, etc)
            $userGroups["group_" . $group->id]["user"] = $userData; // Guarda o registo do Utilizador (Dados do utilizador)
            $userGroups["group_" . $group->id]["group"] = $groupData; // Guarda o registo do Grupo (Dados do grupo)

        }

        return $userGroups;


    }





    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function show(Group $group)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function edit(Group $group)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Group $group)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Group::destroy($id);
        return redirect('/home');
    }
}
