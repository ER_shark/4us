<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Photo;
use Carbon\Carbon;
use Illuminate\Http\Request;


use App\Http\Controllers\GoogleDriveController;
use App\Http\Controllers\UserFriendController;
use App\Http\Controllers\UserController;




use App\User;
use File; // Facade File, para apagar imagem da pasta public


class PhotoController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Photo  $photo
     * @return \Illuminate\Http\Response
     */
    public function showPhotos($username = null)
    {        

        // Vai buscar o utilizador logado

        $current_user = Auth::user();
       
        // Instanciar controlador para utilizar método de outro controlador
        $userController = new UserController();
        $userFriendController = new UserFriendController();
 
        // Vai buscar o utilizador responsável pelo perfil (cujo username está no url)
        $myself = $userController->getUserByUsername($username);


        // Verifica se o utilizador do perfil é amigo do utilizador atual
        $isFriend = $userFriendController->isFriendOf($myself, $current_user);


        if ($myself) {

            if ($isFriend == 1 || $myself->id == $current_user->id) { // Se for amigo dele ou for ele próprio, pode ver os amigos
                $photos = $myself->photos;
                $profile = $this->getProfilePhoto($current_user->id);
                return view('test2.photos', compact('current_user', 'myself', 'profile', 'photos'));
            } else {
                // Se não for amigo, volta para a página anterior
                return redirect()->back();
            }


        } else {
            return view('auth.login');
        }

    }


    
    //funçao que devolve apenas a foto de perfil
    public function getProfilePhoto($user)
    {

        $photos = User::find($user)->photos;

        foreach ($photos as $photo) {
            if ($photo->galery == 'profile') {
                return $photo->url;
            }
        }
        
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Photo  $photo
     * @return \Illuminate\Http\Response
     */
    public function edit(Photo $photo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *Permite alterar a que galeria pertence
     *ex: foto de perfil, mural ou galeria de photo
     */
    public function updatePhotoGalery(Request $request)
    {
        $user = Auth::user()->id;
        $photo = Photo::find($request->photo_id); //acede a foto que está ser modificada
        if ($photo) { //se existir
            //caso a foto seja a do mural ou a de perfil é preciso modificar a atual
            if ($request->photo != "photo") {
                $oldProfilePics = $user->photos; //encontra todas as fotos
                verifyGalery($oldProfilePics,$request);
            }
            //mudamos a foto em edicao para a escolha do user
            $photo->photo = $request->photo;
            $photo->save();
            return redirect()->back();
        }
    }

    public function verifyGalery($oldProfilePics, $request)
    {
        foreach ($oldProfilePics as $oldpic) { //percorre as fotos
            //acede à relacao do model e retona todas as fotos do user
            //se for a nova foto de perfil    
            if ($request->photo == "profile") {
                if ($oldpic->photo == "profile") {
                    $oldpic->photo == "photo"; // a antiga passa a ser 
                    $oldpic->save();
                }
            }
            if ($request->photo == "wall") { //se for a nova foto de perfil    
                if ($oldpic->photo == "wall") {
                    $oldpic->photo == "photo"; // a antiga passa a ser 
                    $oldpic->save();
                }
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Photo  $photo
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Photo::destroy($id);
        return redirect('/home'); //verificar deve devolver a pagina atual
    }

    /**
     * funçao que permite atualizar a inserir novas photos
     */
    public function uploadImages(Request $request)
    {

        $user = Auth::user()->id; // Vai buscar o utilizador que tem sessão iniciada (pois, as fotografias serão dele)
        
        $images = $request->file('files'); // Vai buscar as imagens do input (podem ser múltiplas, logo, guarda em array)
        
        if ($images == null) { // Caso não seja feito upload de nenhuma das imagens
            return redirect()->back()->with('alert', 'Inserir imagem!');  // Redireciona para a página anterior
        }
        

        // Cria instância do controlador do google drive, para utilizar método de upload
        $googledrive = new GoogleDriveController();


        foreach ($images as $img) { // Percorre todas as imagens inseridas, para a guardar

            $userPhoto = new Photo; // Cria nova instância do modelo Photo (nova fotografia)
            $filename = $img->getClientOriginalName(); // Vai buscar o nome do ficheiro
            $filename = time().$filename; // Adicionar timestamp ao nome do ficheiro para evitar ficheiros com mesmo nome
            $userPhoto->user_id = $user; // O dono da foto é o autenticado
            $userPhoto->galery = "photo"; // Indica qual a galeria
            
            if ($img) {

                $img->move('images/', $filename); // Guarda a foto no caminho especificado, temporariamente
                
                // Guarda na pasta files do google drive a fotografia guardada temporiariamente
                $googledrive->putFile('images/', $filename);

                // Remove a fotografia guardada temporariamente, após ter sido guardada no google drive
                File::delete(public_path("images/" . $filename)); // public_path devolve o caminho completo na pasta public

            }

            $userPhoto->url = $googledrive->getImageLink($filename);// Cria o caminho completo da foto para o google drive

            $userPhoto->save();

        }

        return redirect()->back()->with('alert', 'Updated!'); // Redireciona de volta apóss guardar as imagens inseridas
    }
}
