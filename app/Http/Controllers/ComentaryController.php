<?php

namespace App\Http\Controllers;

use App\Comentary;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class ComentaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
    }

    


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
                
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Comentary  $comentary
     * @return \Illuminate\Http\Response
     */
    public function show(Comentary $comentary)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Comentary  $comentary
     * @return \Illuminate\Http\Response
     */
    public function edit(Comentary $comentary)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Comentary  $comentary
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Comentary $comentary)
    {
        //
    }
    /**
     * adiciona um novo comentario
     */
    public function add(Request $request)
    {
        $current_user = Auth::user();
        if ($current_user) {
            $comentary = new Comentary;
            $comentary->comentary =  $request->comentary; // Comentário vindo do formulário (com name comentary)
            $comentary->publication_id = $request->pub_id; // Vindo do formulário o id da publicação
            $comentary->user_id = $current_user->id; // ID do utilizador logado
            $comentary->date = Carbon::now(); //  Data atual
            $comentary->save(); // Guarda na base de dados

            // Redireciona para a página 
            if ($request->current_url == "/publication/search") { // Se estiver na pesquisa, vai para a página inicial
                return redirect('/home');
            } else {
                return redirect()->back(); // Redireciona de volta
            }


        } else {
            return redirect('/');
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Comentary  $comentary
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $current_url)
    {
        Comentary::destroy($id);
        
         // Redireciona para a página 
         if ($current_url == 0) { // Se estiver na pesquisa, vai para a página inicial
            return redirect('/home');
        } else {
            return redirect()->back(); // Redireciona de volta
        }
        
    }
}
