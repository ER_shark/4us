<?php

namespace App\Http\Controllers;

use App\GroupPublication;
use Illuminate\Http\Request;

// Modelos
use App\Group;

class GroupPublicationController extends Controller
{


    /**
     * Esta função permite buscar as publicações de um determinado grupo
     */
    public function getGroupPublications($idGroup) {
        
        // Vai buscar o grupo com este id, e vai buscar as suas publicações (através da relação)
        $groupPublications = Group::find($idGroup)->groupPublications;

        return $groupPublications;

    }




    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\GroupPublication  $groupPublication
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        GroupPublication::destroy($id);
        return redirect('/home');
    }
}
