<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/{login}', function () {
    return view('auth/login');
})->where('login','|login');

Route::get('/register', function () {
    return view('auth/register');
});


Auth::routes();


// Pedidos AJAX
Route::get('/ajax/posts', 'AjaxController@getPosts'); // Buscar 5 posts de cada vez
Route::get('/ajax/like', 'AjaxController@addLike'); // Dar reação numa publicação
Route::get('/ajax/getDiffForHumans', 'AjaxController@getDiffForHumans'); // Devolver a diferença de datas para humanos (dias, meses, anos, etc)




// Pedidos API

// Pedir token para aceder a dados privados
Route::get('/api/token', 'APIController@createToken');

// Utilizadores
Route::get('/api/users/{token}', 'APIController@getAllUsers');

// Publicações
Route::get('/api/publications/{userID}/{token}', 'APIController@getPublicationsOfUser');
Route::get('/api/publications/{token}', 'APIController@getAllPublications');

// Comentários
Route::get('/api/comments/p/{publicationID}/{token}', 'APIController@getCommentsOfPublication');
Route::get('/api/comments/u/{userID}/{token}', 'APIController@getCommentsOfUser');
Route::get('/api/comments/{token}', 'APIController@getAllComments');

// Documentos
Route::get('/api/documents/p/publicationID}/{token}', 'APIController@getDocumentsOfPublication');
Route::get('/api/documents/u/{userID}/{token}', 'APIController@getDocumentsOfUser');
Route::get('/api/documents/{token}', 'APIController@getAllDocuments');

// Amigos
Route::get('/api/friends/{userID}/{token}', 'APIController@getUserFriends');

// Fotos
Route::get('/api/photos/{userID}/{token}', 'APIController@getPhotosOfUser');
Route::get('/api/photos/{token}', 'APIController@getAllPhotos');






// Rota de criar publicação (POST)
Route::post('/pub/create', 'PublicationController@createPublication');

// Rota para procurar publicação pela descrição
Route::post('/publication/search', 'PublicationController@searchPublication')->name('searchPublication');

// Rota de criar publicação num grupo (POST)
Route::post('/group/pub/create', 'PublicationController@createPublication');

// Rota de criar comentário (POST)
Route::post('/home', 'ComentaryController@add');

// Rota de editar perfil (POST)
Route::post('/profile/edit', 'UserController@editProfile');

// Rota de editar grupo (POST)
Route::post('/group/edit', 'GroupController@editGroup');

// Rota para ir buscar as publicações
Route::get('/home', 'PublicationController@getPublication')->name('home');

// Rota para apagar comentário
Route::get('home/comm/{id}/{current_url}','ComentaryController@destroy'); 

// Rota para apagar publicação
Route::get('home/public/{id}/{current_url}','PublicationController@destroy'); 

// Rota para fazer download de um ficheiro
Route::get('document/download/{filename}','DocumentController@downloadDocument'); 

// Rota para aceitar pedido de amizade
Route::get('/friend/accept/{requestID}', 'UserFriendController@acceptRequest'); 

// Rota para recusar pedido de amizade
Route::get('/friend/decline/{requestID}', 'UserFriendController@declineRequest');

// Rota para recusar pedido de amizade
Route::get('/friend/cancel/{requestID}', 'UserFriendController@cancelRequest');

// Rota para adicionar amigo
Route::get('/friend/add/{friendId}', 'UserFriendController@sendFriendRequest');

// Rota para remover amigo
Route::get('/friend/remove/{friendId}', 'UserFriendController@removeFriend');

// Rota para inscrever num grupo
Route::get('/group/join/{groupID}', 'GroupController@joinGroup');

// Rota para sair de um grupo
Route::get('/group/leave/{groupID}', 'GroupController@leaveGroup');

// Rota para tornar um membro em professor ou de professor em membro
Route::get('/group/make/{groupID}/{memberID}/{option}', 'GroupController@makeMemberOrProfessor');

// Rota para aceder a um grupo
Route::get('/group/page/{groupID}', 'GroupController@accessGroup')->name('accessGroup');

// Rota para criar um novo grupo
Route::post('/group/new', 'GroupController@createGroup')->name('createGroup');

// Rota para apagar membro do grupo
Route::get('/member/remove/{groupID}/{memberID}', 'GroupController@removeMember');

// Rota para apagar grupo
Route::get('/group/delete/{groupID}', 'GroupController@deleteGroup');

// Rota para procurar um grupo pelo nome
Route::post('/group/search', 'GroupController@searchGroup')->name('searchGroup');

// Rota para procurar um utilizador pelo nome
Route::post('/user/search', 'UserController@searchUser')->name('searchUser');

// Rota para ver sobre de um grupo
Route::get('/group/about/{groupID}', 'GroupController@showInfo');

// Rota para ver membros de um grupo
Route::get('/group/members/{groupID}', 'GroupController@showMembers');

// Rota para ver documentos de um grupo
Route::get('/group/documents/{groupID}', 'GroupController@showDocuments');



// Rota de fazer múltiplo upload das fotografias do utilizador
Route::post('/file', 'PhotoController@uploadImages')->name('file');




// Rotas do próprio utilizador (que está logado)
Route::get('/profile', 'UserController@getProfile'); // Perfil
Route::get('/about', 'UserController@getInfo'); // Sobre
Route::get('/friends', 'UserFriendController@getFriends'); // Amigos
Route::get('/documents', 'DocumentController@showDocuments'); // Documentos
Route::get('/photos', 'PhotoController@showPhotos'); // Fotos
Route::get('/groups', 'GroupController@showGroups')->name('groupsList'); // Grupos
Route::get('/notifications', 'UserFriendController@showFriendRequests'); // Pedidos de Amizade


// Rotas de outros utilizadores
Route::get('/profile/{username}', 'UserController@getProfile'); // Perfil
Route::get('/about/{username}', 'UserController@getInfo'); // Sobre
Route::get('/friends/{username}', 'UserFriendController@getFriends'); // Amigos
Route::get('/documents/{username}', 'DocumentController@showDocuments'); // Documentos
Route::get('/photos/{username}', 'PhotoController@showPhotos'); // Fotos
Route::get('/groups/{username}', 'GroupController@showGroups'); // Grupos





//Route::get('/home', 'HomeController@index')->name('home');



Route::get('put', function() {
    // Storage::disk('google')->put('test.txt', 'Hello World');

    $filename = '1574794715logo.png';
    $filePath = public_path("images/" . $filename);

    Storage::disk('google')->put($filename, fopen($filePath, 'r+'));

    return 'File was saved to Google Drive';
});


Route::get('list', function() {
    $dir = '/';
    $recursive = false; // Get subdirectories also?
    $contents = collect(Storage::disk('google')->listContents($dir, $recursive));
    //return $contents->where('type', '=', 'dir'); // directories
    return $contents->where('type', '=', 'file'); // files
});



// MAIS INFO: https://github.com/ivanvermeyen/laravel-google-drive-demo/blob/master/routes/web.php
Route::get('get', function() {
    $filename = 'test.txt';
    $dir = '/';
    $recursive = false; // Get subdirectories also?
    $contents = collect(Storage::disk('google')->listContents($dir, $recursive));
    $file = $contents
        ->where('type', '=', 'file')
        ->where('filename', '=', pathinfo($filename, PATHINFO_FILENAME))
        ->where('extension', '=', pathinfo($filename, PATHINFO_EXTENSION))
        ->first(); // there can be duplicate file names!
    //return $file; // array with file info
    $rawData = Storage::disk('google')->get($file['path']);
    return response($rawData, 200)
        ->header('ContentType', $file['mimetype'])
        ->header('Content-Disposition', "attachment; filename='$filename'");
});



Route::get('/nuno', function () {

    $img = Image::make('images/pdf.png')->resize(320, 240);
    $img->save('images/pdfx.png');

    return "done";

});