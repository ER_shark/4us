$(document).ready(function (e) {


    // Inicialmente, quando a página carrega, o formulário de editar perfil está escondido (só aparece ao clicar no botão)
    // Cada vez que o botão é clicado , ele alterna entre esconder e mostrar
    var hidden = true;
    $("#editProfile").hide();
    $("#showProfile").show();

    if (window.location.pathname == "/home" || (window.location.pathname).includes("/profile") || (window.location.pathname).includes("/group/page") ) { // Faz apenas na página home

        $(window).scroll(function() { // Ao fazer scroll
            
            if($(window).scrollTop() + $(window).height() == $(document).height()) { // Ao chegar ao fim da página
                
                
                var option = 0;
                if (window.location.pathname == "/home") {
                    option = 1; // Buscar todos os posts dele e de amigos (a partir do index)
                } else if ((window.location.pathname).includes("/profile")) {
                    option = 2; // Buscar todos os posts apenas do dono do perfil (a partir do index)
                } else if ((window.location.pathname).includes("/group/page")) {
                    option = 3; // Buscar todos os posts do grupo (a partir do index)
                }

                
                // Buscar mais 5 posts
                $.ajax({
                    type: 'get',
                    url: '/ajax/posts',
                    async: false, // O resto do código não é executado até estar concluído
                    dataType: 'json',
                    data: { 
                        '_token': '<?php echo csrf_token() ?>', 
                        'option': option,
                        'groupID': $("#groupID").text(), // Id do grupo (caso haja)
                        'userProfileID': $("#userProfileID").text(),
                        'start': parseInt($("#start").text()) // A partir de onde vai carregar novas publicações
                    },
                    success: function(data) {
                        
                        $("#start").text(data.last); // Atualiza o start, para saber qual o próximo a começar, na próxima vez que chegar ao fim da página

                        $.each(data.publications, function(index, pub) {

                            var pub_date = pub['pub_date'];

                            $.ajax({
                                type: 'get',
                                url: '/ajax/getDiffForHumans',
                                async: false, // O resto do código não é executado até estar concluído
                                data: { 
                                    '_token': '<?php echo csrf_token() ?>', 
                                    'date': pub_date
                                },
                                success: function(data) {


                                    var choosenClass = "";
                                    if (window.location.pathname == "/home") {
                                        choosenClass = "<div class='post'>";
                                    } else {
                                        choosenClass = "<div class='profile_post'>";
                                    }


                                    var publication = choosenClass + '\
                                        <div class="lado_esquerdo">\
                                            <img src="' + pub['user_photo'] + '" id="imagem_utilizador2">\
                                        </div>\
                                        \
                                        <div class="lado_direito">\
                                            <h3><a href="personal-profile.html" title="Profile">' + pub['user_name'] + '</a></h3>\
                                            <p><strong><i>há ' + data.replace(" depois", "") + '</i></strong></p>\
                                            <p class="descricao">' + pub['pub_description'] + '</p>';


                                            // Documentos
                                            publication += '<ul class="documents_list"></ul>';
                                                $.each(pub['documents'], function(index, doc) {
                                                    publication += '<li><a href="#">' + doc['path'] + '</a></li>';
                                                });
                                            publication += '</ul>\
                                            <br>';
                                        

                                            var n_comentaries = 0;


                                            // Contar o nº de comentários
                                            $.each(pub['comentaries'], function(index, doc) {
                                                n_comentaries++;
                                            });


                                            publication += '<div class="emojis">\
                                                <img alt="Gosto" val="0" pub="' + pub['id'] + '" title="Gosto" src="https://twemoji.maxcdn.com/16x16/1f603.png"> <span pub="' + pub['id']+ '" class="pubLikes">' + pub['likes'][0] + '</span>&nbsp;&nbsp;&nbsp;&nbsp;\
                                                <img alt="Adoro" val="1" pub="' + pub['id'] + '" title="Adoro" src="https://twemoji.maxcdn.com/16x16/1f60d.png"> <span pub="' + pub['id'] + '" class="pubLoves">' + pub['likes'][1] + '</span>&nbsp;&nbsp;&nbsp;&nbsp;\
                                                <img alt="Não Gosto" val="2" pub="' + pub['id'] + '" title="Não Gosto" src="https://twemoji.maxcdn.com/16x16/1f623.png"> <span pub="' + pub['id'] + '" class="pubDislikes">' + pub['likes'][2] + '</span>\
                                            </div>\
                                            <br>\
                                            <hr>';

                                            if (n_comentaries > 0) { // Se existirem comentários na publicação

                                                publication += '<div id="comments_box" style="height: 200px; overflow: scroll;">';
                                                
                                                // Comentários
                                                $.each(pub['comentaries'], function(index, comment) {
                                                    $.ajax({
                                                        type: 'get',
                                                        async: false, // O resto do código não é executado até estar concluído
                                                        url: '/ajax/getDiffForHumans',
                                                        data: { 
                                                            '_token': '<?php echo csrf_token() ?>', 
                                                            'date': comment['com_date']
                                                        },
                                                        success: function(data) {

                                                            publication += '<p class="comment">\
                                                            <img style="float: left; margin-right: 15px;" class="comment_user_photo" src="' + comment['com_user_photo']+ '" />\
                                                            <span class="comment_user"><a class="simpleHref" href="/profile/' + comment['com_user_username'] + '">' + comment['com_user_name'] + '</a>';
                                                                
                                                                if (comment['com_user_id'] == parseInt($("#current_user").text())) {
                                                                    publication += '<a href="home/comm/' + comment['com_id']  + '"><span class="fa fa-times deleteComment"></span></a><br>';
                                                                }
    
                                                            publication += '</span>\
                                                            <br />\
                                                            <span class="comment_date">há ' + data.replace(" depois", "") + '</span>\
                                                            <br /><br />\
                                                            <span class="comment_description">' + comment['com_description'] + '</span>\
                                                        </p>';

                                                        
                                                        }

                                                    })

                                                  
                                                });

                                                publication += '</div>';

                                            } else  { // Se não existirem comentários na publicação

                                                publication += '<span style="font-size: 13px;">Sem comentários. Seja o primeiro!</span>';

                                            }
                                            

                                            publication += '<br><br>\
                                            <form method="post" action="/home">\
                                                <input type="hidden" name="_token" value="' + $("#token").text() + '">\
                                                <input type="hidden" name="pub_id" value="' + pub['id'] + '">\
                                                <input type="text" name="comentary" class="caixa_comentario" placeholder="Adicionar comentário">\
                                                <button type="submit">Add Comentary</button>\
                                        \
                                            </form>\
                                        \
                                        </div>\
                                        \
                                        \
                                        </div>';


                                        // Como async = false, só faz isto após todos os pedidos ajax serem feitos
                                        if (window.location.pathname == "/home") {
                                            $("#feed").append(publication);
                                        } else {
                                            $("#left_side_profile").append(publication);
                                        }
                                        


                                }
                            });


                        });
                        
                    }
                });

            }
        });

    }
    
    $("#addPhotoButton").click(function (e) {
        e.preventDefault();
        $("#myModal").css('display', 'block');
    });


    $(".close:eq(0)").click(function (e) {
        e.preventDefault();
        $("#myModal").css('display', 'none');
    });
    

    $("#addGroupButton").click(function (e) {
        e.preventDefault();
        $("#groupModal").css('display', 'block');
    })


    $(".groupModalClose").click(function (e) {
        e.preventDefault();
        $("#groupModal").css('display', 'none');
    });


    $("#editProfileButton").click(function (e) {
       
        e.preventDefault(); // Ignora a ação do href

        if (hidden == true) {
            // Mostra formulário de editar
            $("#editProfile").show();
            // Esconde a visualização do perfil
            $("#showProfile").hide();
            hidden = false;
        } else {
             // Esconde formulário de editar
             $("#editProfile").hide();
             // Mostra visualização do perfil
             $("#showProfile").show();
             hidden = true;
        }

    });



    $(".emojis img").click(function (e) {

        var val = $(this).attr('val'); // Buscar o tipo de reação dado (atributo val que cada imagem tem)
        var pub = $(this).attr('pub'); // Buscar a publicação em que o utilizador está a atribuir reação

        $.ajax({
            type: 'get',
            url: '/ajax/like',
            dataType: 'json',
            data: { 
                '_token': '<?php echo csrf_token() ?>', 
                'val': val,
                'pub': pub
            },
            success: function(data) {
                
                // Atualiza as reações da publicação após ser feita nova reação nessa publicação
                $(".pubLikes[pub=" + pub + "]").text(data[0]);
                $(".pubLoves[pub=" + pub + "]").text(data[1]);
                $(".pubDislikes[pub=" + pub + "]").text(data[2]);

            }
        });


    });

    

});