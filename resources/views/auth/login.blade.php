@extends('layouts.auth')
@section('title','Login')

@section('content')
<div id="logo_pagina_principal">
    <p><img src="{{asset('images/logo.png')}}" alt="logo"></p>
</div>
<form method="POST" action="{{ route('login') }}">
    <input type="hidden" name="_token" value="{{csrf_token()}}">
    <input name='email' type="email" value="{{old('email')}}" class="formulario_login" placeholder="Email"><br><br>
    @error('email')
        <strong class="erro_autenticacao">{{ $message }}</strong>
    @enderror
    <input name='password' type="password" class="formulario_login" placeholder="Palavra-passe"><br><br>
    @error('password')
        <strong class="erro_autenticacao">{{ $message }}</strong>
    @enderror
    <input type="submit" id="botao-login" value="ENTRAR">
</form>
<a href="/register" class="link_pagina_principal">
    <p>Ainda não tem conta? Registe-se!</p>
</a>
@endsection
