@extends('layouts.auth')
@section('title','Registo')
@section('content')
<div id="logo_pagina_principal">
    <p><img src="{{asset('images/logo.png')}}" alt="logo"></p>
</div>
<form method="POST" action="{{ route('register') }}">
    <input type="hidden" name="_token" value="{{csrf_token()}}">
    <input name='name' type="text" value="{{old('name')}}" class="formulario_login" placeholder="Nome de utilizador">
    @error('name')
        <strong class="erro_autenticacao">{{ $message }}</strong>
    @enderror
    <br><br>
    <input name='email' type="email" value="{{old('email')}}" class="formulario_login" placeholder="Email">
    @error('email')
        <strong class="erro_autenticacao">{{ $message }}</strong>
    @enderror
    <br><br>
    <input name='password' type="password"class="formulario_login" placeholder="Palavra-passe">
    @error('password')
        <strong class="erro_autenticacao">{{ $message }}</strong>
    @enderror
    <br><br>
    <input name='repeat_password' type="password" class="formulario_login" placeholder="Confirmar Palavra-passe">
    @error('repeat_password')
        <strong class="erro_autenticacao">{{ $message }}</strong>
    @enderror
    <span id="guardarpasse"><input type="checkbox"> Guardar palavra-passe</span>
    <input type="submit" id="botao-login" value="REGISTAR">
</form>
<a href="/login" class="link_pagina_principal">
    <p>Já tem conta? Iniciar sessão!</p>
</a>
@endsection
