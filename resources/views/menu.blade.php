<html>
    <head>
    <link href="{{asset('css/home.css')}}" rel="stylesheet">
    </head>
    <body>
    
        <div id="banner"></div>

        <?php 
        function isActive($currentUrl) {
            $url = $_SERVER['REQUEST_URI'];
            
            if (strpos($url, $currentUrl) !== false) {
                return "class='active'";
            }

            return "";
        }
        ?>

        <ul id="profile_menu">
            <li <?=isActive('/profile');?>><a href="<?=str_replace(explode("/", $_SERVER['REQUEST_URI'])[1], "profile", $_SERVER['REQUEST_URI'])?>">Perfil</a></li>
            <li <?=isActive('/about');?>><a href="<?=str_replace(explode("/", $_SERVER['REQUEST_URI'])[1], "about", $_SERVER['REQUEST_URI'])?>">Sobre</a></li>
            <li <?=isActive('/friends');?>><a href="<?=str_replace(explode("/", $_SERVER['REQUEST_URI'])[1], "friends", $_SERVER['REQUEST_URI'])?>">Amigos</a></li>
            <li <?=isActive('/documents');?>><a href="<?=str_replace(explode("/", $_SERVER['REQUEST_URI'])[1], "documents", $_SERVER['REQUEST_URI'])?>">Documentos</a></li>
            <li <?=isActive('/photos');?>><a href="<?=str_replace(explode("/", $_SERVER['REQUEST_URI'])[1], "photos", $_SERVER['REQUEST_URI'])?>">Fotos</a></li>
            <li <?=isActive('/groups');?>><a href="<?=str_replace(explode("/", $_SERVER['REQUEST_URI'])[1], "groups", $_SERVER['REQUEST_URI'])?>">Grupos</a></li>
        </ul>

    </body>
</html>