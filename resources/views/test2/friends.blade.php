@extends('test2.base')

@section('content')

<div id="friends_list">

    <div id="menu">
        @include('menu')
    </div>

        <!-- Apenas pode procurar pessoas ou caso esteja no seus próprios amigos -->
        @if ($myself->id == $current_user->id)

            <form method="post" action="{{ route('searchUser') }}">

                {{ csrf_field() }}

                <div class="alignCenter">
                    <button id="searchUserButton" type="submit"><i class="fa fa-search"></i></button>
                </div>

                <input id="groupSearchBox" name="search" type="text" placeholder="Pesquisar Utilizador (Nome/Username)..."><br><br>
            </form><br><br>

        @endif

    <div id="scrollbar">


        @foreach ($friends  as $friend)
        
    
        <div class="friend">
            <div class="friend_image">
            <img src="{{$friend['photo']}}" alt="{{$friend['name']}}" class="photo_user">
            </div>
            <div class="friend_info">
                <p><strong>{{$friend['name']}}</strong></p>

                <p class="chat-time">{{$friend['user_name']}}</p>
                <p><?=substr($friend['description'],0, 70) . '...'?></p>
            </div>
            <div class="friend_chat">
                <p><a href="profile/{{ $friend['user_name'] }}" title="Ver perfil"><span class="see_profile">Ver perfil &gt;</span></a></p>
                @if ($option == 1)
                    <p><a href="friend/remove/{{ $friend['id'] }}" title="Ver perfil"><span class="see_profile">Remover Amigo &gt;</span></a></p>
                @endif
            </div>
        </div>

        @endforeach

       



    </div>

</div>

@endsection