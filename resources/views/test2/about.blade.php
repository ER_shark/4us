@extends('test2.base')

@section('content')

<div id="about_section">

    <div id="menu">
        @include('menu')
    </div>

    <div id="info">
            
        <div class="card-post">


            <!-- Apenas permite editar o seu próprio perfil e não dos outros -->
            @if ($myself->id == $current_user->id)
                <div class="alignCenter">
                    <a id="editProfileButton" href='#'>
                        <i class="fa fa-pencil"></i>
                    </a>
                </div>
            @endif

            <div id="showProfile">

                <table style="width: 80%; margin: 0 auto;">

                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>

                    <tr>
                        <td><strong>Nome</strong></td>
                        <td>{{$myself['name']}}</td>
                    </tr>

                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>

                    <tr>
                        <td><strong>Função</strong></td>
                        <td>{{$myself['role']['role']}}</td> 
                    </tr>

                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>

                    <tr>
                        <td><strong>Estudos</strong></td>
                        <td>
                            @foreach ($myself['userDegrees'] as $degree)
                                Grau: {{$degree['degrees']}}<br>
                                Começo: {{$degree['date_begin']}}<br>
                                Fim: {{$degree['date_end']}}<br>
                            @endforeach
                        </td>
                    </tr>

                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>

                    <tr>
                        <td><strong>Telemóvel</strong></td>
                        <td>{{$myself['phone']}}</td>
                    </tr>

                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>

                    <tr>
                        <td><strong>Descrição</strong></td>
                        <td>{{$myself['description']}}</td>
                    </tr>

                </table>


            </div>

            
            <div id="editProfile">

                <form method="POST" action="/profile/edit">
                    <table style="width: 80%; margin: 0 auto;">

                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="user_id" value="{{$myself['id']}}">

                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>

                        <tr>
                            <td><strong>Nome</strong></td>
                            <td><input class="input" type="text" name="name" value="{{$myself['name']}}"/></td>
                        </tr>

                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>

                        <tr>
                            <td><strong>Username</strong></td>
                            <td><input class="input" type="text" name="username" value="{{$myself['user_name']}}"/></td>
                        </tr>

                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>

                        <tr>
                            <td><strong>Estudos</strong></td>
                            <td>
                                @foreach ($myself['userDegrees'] as $degree)
                                    <input type="hidden" name="degree_id[]" value="{{$degree['id']}}"/>
                                    <strong>Grau (<span class="littleInfo"><u>APENAS:</u> Estudante / Licenciado / Mestrado / Doutorado / Outro):</span></strong> <input class="input" type="text" name="degree[]" value="{{$degree['degrees']}}"/><br>
                                    <strong>Começo:</strong> <input class="input" type="text" name="begin[]" value="{{$degree['date_begin']}}"/><br>
                                    <strong>Fim:</strong> <input class="input" type="text" name="end[]" value="{{$degree['date_end']}}"/><br>
                                @endforeach
                            </td>
                        </tr>

                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>

                        <tr>
                            <td><strong>Telemóvel</strong></td>
                            <td><input class="input" type="text" name="phone" value="{{$myself['phone']}}"/></td>
                        </tr>

                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>

                        <tr>
                            <td><strong>Email</strong></td>
                            <td><input disabled class="input" type="email" name="email" value="{{$myself['email']}}"/></td>
                        </tr>

                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>

                        <tr>
                            <td><strong>Descrição</strong></td>
                            <td><textarea class="input" name="description">{{$myself['description']}}</textarea></td>
                        </tr>

                        <tr>
                            <td>&nbsp;</td>
                            <td><input id="saveProfileButton" type="submit" value="Save"></input></td>
                        </tr>

                    </table>
                </form>

            </div>
        
        </div>

    </div>

</div>

@endsection