@extends('test2.base')

@section('content')

<div id="profile_content">

        @if ($publicationsOfGroup != null)
            <!-- A próxima publicação a mostrar terá de começar após estas primeiras que são mostradas -->
            <span style="display: none;" id="start"> {{ count(array_slice($publicationsOfGroup, 0, 5, true)) }} </span> <!-- Permite guardar a partir de qual publicação irá carregar ao chegar ao fim da página -->
        @else 
            <span style="display: none;" id="start">0</span> <!-- Permite guardar a partir de qual publicação irá carregar ao chegar ao fim da página -->
        @endif
        <span style="display: none;" id="current_user"> {{ $current_user->id }} </span> <!-- Permite guardar a partir de qual publicação irá carregar ao chegar ao fim da página -->
        <span style="display: none;" id="token"> {{ csrf_token() }} </span> <!-- Guarda o token -->
        <span style="display: none;" id="groupID"> {{ $groupID }} </span> <!-- Guarda o id do grupo -->

    <div id="menu">
        @include('menu_group')
    </div>

    <div id="content">
    
        <div id="left_side_profile">

        
            <!-- Apenas pode adicionar um post caso seja professor/dono no grupo (se tiver permissões) -->
            @if ($userHasPermissions)
            
                <div id="add_new_post" class="profile_post">
                    <br>
                    <form method="post" action="pub/create" enctype="multipart/form-data">
                        <textarea id="post_content" name="description" required rows="8" style="width: 85%;" placeholder="Digite a sua publicação"></textarea>
                        <input type="hidden" name="group_id" value="{{ $groupID }}"><br>
                        <input type="hidden" name="_token" value="{{csrf_token()}}"><br>
                        <input type="hidden" name="current_url" value="<?=$_SERVER['REQUEST_URI']?>"/>
                        <label for="file-upload" class="custom-file-upload">
                            <i class="fa fa-cloud-upload"></i> Inserir Documentos
                        </label>
                        <input id="file-upload" type="file" name="files[]" multiple>
                        <label for="pub-add" class="add-pub-button">
                            <i class="fa fa-cloud-upload"></i> Adicionar Publicação
                        </label>
                        <input type="submit" id="pub-add" class="addPost" value="Inserir">
                    </form>
                    <br>
                </div>
            
            @endif  



            @if ($publicationsOfGroup != null)
                
                @foreach (array_slice($publicationsOfGroup, 0, 5, true) as $pub)

                    <div class="profile_post">

                        <div class="lado_esquerdo">
                            <img src="{{$pub['user_photo']}}" id="imagem_utilizador2">
                        </div>

                        <div class="lado_direito"><?php Carbon\Carbon::now()->setlocale('pt') ?>

                            <!-- Se for o dono da publicação, professor no grupo ou o dono do grupo, pode apagar -->
                            @if($pub['user_id'] == $current_user->id || $userHasPermissions || $current_user->role_id == 1)
                                @if ($_SERVER['REQUEST_URI'] == "/publication/search")
                                    <a href="home/public/{{$pub['id']}}/0"><span class="fa fa-times deletePublication"></span></a><br>
                                @else
                                    <a href="home/public/{{$pub['id']}}/1"><span class="fa fa-times deletePublication"></span></a><br>
                                @endif
                            @endif

                            <h3><a href="/profile/{{ $pub['user_username'] }}" title="Profile">{{$pub['user_name']}}</a></h3>
                            <p><strong><i>há {{ (Carbon\Carbon::now())->diffForHumans(Carbon\Carbon::parse($pub['pub_date']), Carbon\CarbonInterface::DIFF_ABSOLUTE) }}</i></strong></p>
                            <p class="descricao">{{$pub['pub_description']}}</p>
                            
                            @if (isset($pub['documents']))
                                <ul class="documents_list">
                                    @foreach ($pub['documents'] as $doc)
                                        <li><a href="document/download/{{ $doc['path'] }}">{{ $doc['path'] }}</a></li>
                                    @endforeach
                                </ul>
                                <br>
                            @endif

                            <div class="emojis">
                                <img alt="Gosto" val="0" pub="{{ $pub['id'] }}" title="Gosto" src="https://twemoji.maxcdn.com/16x16/1f603.png"> <span pub="{{ $pub['id'] }}" class="pubLikes">{{ $pub['likes'][0] }}</span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <img alt="Adoro" val="1" pub="{{ $pub['id'] }}" title="Adoro" src="https://twemoji.maxcdn.com/16x16/1f60d.png"> <span pub="{{ $pub['id'] }}" class="pubLoves">{{ $pub['likes'][1] }}</span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <img alt="Não Gosto" val="2" pub="{{ $pub['id'] }}" title="Não Gosto" src="https://twemoji.maxcdn.com/16x16/1f623.png"> <span pub="{{ $pub['id'] }}" class="pubDislikes">{{ $pub['likes'][2] }}</span>
                            </div>
                            <br>
                            <hr>
                            @if (isset($pub['comentaries']))
                            <div id="comments_box" style="height: 200px; overflow: scroll;">
                                    @foreach ($pub['comentaries'] as $comentary)
                                    <p class="comment">
                                        <img style="float: left; margin-right: 15px;" class="comment_user_photo" src="{{$comentary['com_user_photo']}}" />
                                        <span class="comment_user"><a class="simpleHref" href="/profile/{{ $comentary['com_user_username'] }}">{{ $comentary['com_user_name'] }}</a>
                                            
                                            <!-- Se for dono do comentário, professor no grupo ou dono do grupo, pode apagar -->
                                            @if ($comentary['com_user_id'] == $current_user->id || $userHasPermissions || $current_user->role_id == 1)
                                                @if ($_SERVER['REQUEST_URI'] == "/publication/search")
                                                    <a href="home/comm/{{$comentary['com_id']}}/0"><span class="fa fa-times deleteComment"></span></a><br>
                                                @else
                                                    <a href="home/comm/{{$comentary['com_id']}}/1"><span class="fa fa-times deleteComment"></span></a><br>
                                                @endif
                                            @endif
                                        </span>
                                        <br />
                                        <span class="comment_date">há {{ (Carbon\Carbon::now())->diffForHumans(Carbon\Carbon::parse($comentary['com_date']), Carbon\CarbonInterface::DIFF_ABSOLUTE) }}</span>
                                        <br /><br />
                                        <span class="comment_description">{{$comentary['com_description']}}</span>
                                    </p>
                                    @endforeach
                            </div>
                            @else
                                <span style="font-size: 13px;">Sem comentários. Seja o primeiro!</span>
                            @endif
                            <br><br>
                            <form method="post" action="/home">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="pub_id" value="{{$pub['id']}}">
                                <input type="hidden" name="current_url" value="<?=$_SERVER['REQUEST_URI']?>"/>
                                <input type="text" name="comentary" class="comments_box" placeholder="Adicionar comentário">
                                <button type="submit">Adicionar Comentário</button>

                            </form>

                        </div>


                    </div>

                @endforeach

            @endif


        </div>


        <div id="right_side_profile">
            
            <div class="profile_section">
                <div style="margin-top: 5%; margin-left: 5%;"><strong>Sobre</strong></div>

                    <!-- O dono do grupo não pode sair do grupo, apenas pode apagar o grupo, e assim remover todos os membros e todas as publicações -->
                    @if ($current_user->id != $ownerID)
                        <!-- Pode sair do grupo caso esteja inscrito no grupo (caso seja membro do grupo) -->
                        <!-- Só tem acesso  a isto se for membro, caso contrário, não consegue entrar no grupo -->
                        <p class="alignCenter">
                            <a href="group/leave/{{ $groupID  }}" title="Sair do Grupo"><span class="send_friend_request">Sair do Grupo </span></a>
                        </p>
                    @else
                        <!-- Apagar grupo -->
                        <p class="alignCenter">
                            <a href="group/delete/{{ $groupID  }}" title="Apagar Grupo"><span class="send_friend_request">Apagar Grupo </span></a>
                        </p>
                    @endif

                <img style="width: 90%; height: auto; padding: 5%;" src="https://socialyte.codeplus.it/img/company.jpg" alt="Suggested post">
            </div>

            <div class="profile_section">

                <div style="margin-top: 5%; margin-left: 5%; margin-bottom: 3%;"><strong>Membros</strong></div>

                <!-- Mostrar apenas 8 amigos -->
                @foreach (array_slice($members->toArray(), 0, 8, true) as $friend)

                    <a style="text-decoration: none;" href="/profile/{{ $friend['user_name'] }}">
                        <img src="{{ $friend['photo'] }}" alt="{{ $friend['name'] }}"  class="profile_friend_photo">
                    </a>

                @endforeach

            </div>


            <div class="profile_section">
                <div style="margin-top: 5%; margin-left: 5%; margin-bottom: 5%;"><strong>Documentos Partilhados</strong></div>
                
                <ul>
                    
                    <!-- Mostrar apenas 5 documentos -->
                    @foreach (array_slice($documents->toArray(), 0, 5, true) as $document)

                        <li style="margin-bottom: 4%;">
                            <a style="background-color: #4f5467; color: #fff; border-radius: 10px; text-align: center; vertical-align: middle; font-size: 14px; padding: 4px 8px; font-weight: normal; text-decoration: none;" href="document/download/{{ $document['path'] }}">
                                {{ $document['path'] }}
                            </a>
                        </li>

                    @endforeach

                </ul>

            </div>

        </div>

    </div>

</div>

@endsection