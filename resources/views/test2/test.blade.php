@extends('test2.base')

@section('content')

    <div id="feed">

        <!-- A próxima publicação a mostrar terá de começar após estas primeiras que são mostradas -->
        <span style="display: none;" id="start"> {{ count(array_slice($publications, 0, 5, true)) }} </span> <!-- Permite guardar a partir de qual publicação irá carregar ao chegar ao fim da página -->
        <span style="display: none;" id="current_user"> {{ $current_user->id }} </span> <!-- Permite guardar a partir de qual publicação irá carregar ao chegar ao fim da página -->
        <span style="display: none;" id="token"> {{ csrf_token() }} </span> <!-- Guarda o token -->

        <div class="profile_post post">
            
            <strong><div style="display: inline-block; width: 25%; margin-left: 4%;">{{ $msg }}</div></strong>

            <div style="display: inline-block; width: 65%;">
            <form method="post" action="{{ route('searchPublication') }}">

                {{ csrf_field() }}

                <div class="alignCenter">
                    <button id="searchPublicationButton" type="submit"><i class="fa fa-search"></i></button>
                </div>

                <input id="publicationSearchBox" name="search" type="text" placeholder="Pesquisar Publicação..."><br><br>
            </form>

        </div>

    </div>
        
        <div id="add_new_post" class="post">
            <br>
            <form method="post" action="pub/create" enctype="multipart/form-data">
                <textarea id="post_content" name="description" required rows="8" style="width: 85%;" placeholder="Digite a sua publicação"></textarea>
                <input type="hidden" name="_token" value="{{csrf_token()}}"><br>
                <input type="hidden" name="current_url" value="<?=$_SERVER['REQUEST_URI']?>"/>
                <label for="file-upload" class="custom-file-upload">
                    <i class="fa fa-cloud-upload"></i> Inserir Documentos
                </label>
                <input id="file-upload" type="file" name="files[]" multiple>
                <label for="pub-add" class="add-pub-button">
                    <i class="fa fa-cloud-upload"></i> Adicionar Publicação
                </label>
                <input type="submit" id="pub-add" class="addPost" value="Inserir">
            </form>
            <br>
        </div>


        @foreach (array_slice($publications, 0, 5, true) as $pub)
        <div class="post">

            <div class="lado_esquerdo">
                <img src="{{$pub['user_photo']}}" id="imagem_utilizador2">
            </div>

            <div class="lado_direito"><?php Carbon\Carbon::now()->setlocale('pt') ?>
                @if($pub['user_id'] == $current_user->id || $current_user->role_id == 1)
                    @if ($_SERVER['REQUEST_URI'] == "/publication/search")
                        <a href="home/public/{{$pub['id']}}/0"><span class="fa fa-times deletePublication"></span></a><br>
                    @else
                        <a href="home/public/{{$pub['id']}}/1"><span class="fa fa-times deletePublication"></span></a><br>
                    @endif
                @endif

                <h3><a href="/profile/{{ $pub['user_username'] }}" title="Profile">{{$pub['user_name']}}</a></h3>
                <p><strong><i>há {{ (Carbon\Carbon::now())->diffForHumans(Carbon\Carbon::parse($pub['pub_date']), Carbon\CarbonInterface::DIFF_ABSOLUTE) }}</i></strong></p>
                <p class="descricao">{{$pub['pub_description']}}</p>

                @if (isset($pub['documents']))
                    <ul class="documents_list">
                        @foreach ($pub['documents'] as $doc)
                            <li><a href="document/download/{{ $doc['path'] }}">{{ $doc['path'] }}</a></li>
                        @endforeach
                    </ul>
                    <br>
                @endif

                <div class="emojis">
                    <img alt="Gosto" val="0" pub="{{ $pub['id'] }}" title="Gosto" src="https://twemoji.maxcdn.com/16x16/1f603.png"> <span pub="{{ $pub['id'] }}" class="pubLikes">{{ $pub['likes'][0] }}</span>&nbsp;&nbsp;&nbsp;&nbsp;
                    <img alt="Adoro" val="1" pub="{{ $pub['id'] }}" title="Adoro" src="https://twemoji.maxcdn.com/16x16/1f60d.png"> <span pub="{{ $pub['id'] }}" class="pubLoves">{{ $pub['likes'][1] }}</span>&nbsp;&nbsp;&nbsp;&nbsp;
                    <img alt="Não Gosto" val="2" pub="{{ $pub['id'] }}" title="Não Gosto" src="https://twemoji.maxcdn.com/16x16/1f623.png"> <span pub="{{ $pub['id'] }}" class="pubDislikes">{{ $pub['likes'][2] }}</span>
                </div>
                <br>
                <hr>
                @if (isset($pub['comentaries']))
                <div id="comments_box" style="height: 200px; overflow: scroll;">

                    @foreach ($pub['comentaries'] as $comentary)
                        <p class="comment">
                            <img style="float: left; margin-right: 15px;" class="comment_user_photo" src="{{$comentary['com_user_photo']}}" />
                            <span class="comment_user"><a class="simpleHref" href="/profile/{{ $comentary['com_user_username'] }}">{{ $comentary['com_user_name'] }}</a>
                                @if ($comentary['com_user_id'] == $current_user->id || $current_user->role_id == 1))
                                    @if ($_SERVER['REQUEST_URI'] == "/publication/search")
                                        <a href="home/comm/{{$comentary['com_id']}}/0"><span class="fa fa-times deleteComment"></span></a><br>
                                    @else
                                        <a href="home/comm/{{$comentary['com_id']}}/1"><span class="fa fa-times deleteComment"></span></a><br>
                                    @endif
                                @endif
                            </span>
                            <br />
                            <span class="comment_date">há {{ (Carbon\Carbon::now())->diffForHumans(Carbon\Carbon::parse($comentary['com_date']), Carbon\CarbonInterface::DIFF_ABSOLUTE) }}</span>
                            <br /><br />
                            <span class="comment_description">{{$comentary['com_description']}}</span>
                        </p>
                        @endforeach

                    </div>
                @else
                    <span style="font-size: 13px;">Sem comentários. Seja o primeiro!</span>
                @endif
                <br><br>
                <form method="post" action="/home">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="pub_id" value="{{$pub['id']}}">
                    <input type="hidden" name="current_url" value="<?=$_SERVER['REQUEST_URI']?>"/>
                    <input type="text" name="comentary" class="comments_box" placeholder="Adicionar comentário">
                    <button type="submit">Adicionar Comentário</button>

                </form>

            </div>


        </div>

        @endforeach

    </div>

@endsection
