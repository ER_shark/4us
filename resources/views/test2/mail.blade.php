<strong><h3 style="color: red;">NOVA INFORMAÇÃO</h3></strong>

<p>
<strong>{{ $teacher_name }}</strong> publicou uma nova informação no grupo <strong>{{ $group_name }}</strong><br> 
({{ $pub_date }}):<br>
<br>
{{ $publication }}
</p>

