@extends('test2.base')

@section('content')

<div id="friends_list">

    <div id="menu">
        @include('menu_group')
    </div>

    <div id="scrollbar">
    @foreach ($groupMembers as $member)
        
    
        <div class="friend">
            <div class="friend_image">
            <img src="{{ $member['photo'] }}" alt="{{ $member['name'] }}" class="photo_user">
            </div>
            <div class="friend_info">
                <p><strong><a class="simpleHref" href="profile/{{ $member['user_name'] }}">{{ $member['name'] }}</a></strong></p>

                <p class="chat-time">{{ $member['user_name'] }} <span class="groupUserRole">({{ $member['role'] }})</span></p>
                <p><?=substr($member['description'],0, 70) . '...'?></p>
            </div>
            <div class="friend_chat">

                <!-- Apenas o dono do grupo tem permissões para remover membros (e não pode remover-se a si próprio) -->
                @if ($ownerID == $current_user->id && $ownerID != $member['id'])

                    @if ($member['role'] == "member")
                        <p><a href="group/make/{{ $groupID }}/{{ $member['id'] }}/1" title="Tornar Professor"><span class="see_profile">Tornar Professor &gt;</span></a></p>
                    @elseif ($member['role'] == "professor")
                        <p><a href="group/make/{{ $groupID }}/{{ $member['id'] }}/2" title="Tornar Membro"><span class="see_profile">Tornar Membro &gt;</span></a></p>
                    @endif

                    <p><a href="member/remove/{{ $groupID }}/{{ $member['id'] }}" title="Remover membro"><span class="see_profile">Remover membro &gt;</span></a></p>
                
                
                @endif
            
            </div>
        </div>

        @endforeach

       



    </div>

</div>

@endsection