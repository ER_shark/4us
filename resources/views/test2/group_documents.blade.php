@extends('test2.base')

@section('content')

<div id="about_section">

    <div id="menu">
        @include('menu_group')
    </div>

    <div id="info">
            
        <div class="card-post">
            <ul class="profile-data">
                @if (count($documents) == 0) <!-- Caso não tenha feito upload de documentos -->
                    Não possui documentos.
                @else <!-- Caso tenha documentos que fez upload -->
                    @foreach ($documents as $doc) <!-- Vai buscar todos os seus documentos e verifica a extensão para cada -->

                        <!-- Dependendo da extensão (tipo de ficheiro), mostra uma imagem diferente -->
                        @php
                            $name = "<a href='document/download/{{ $doc[path] }}'>$doc[path]</a>";
                        @endphp

                        @if (strpos($doc['path'], '.pdf') !== false)
                            <span class="document" ><img src="images/pdf.png" width="100px" height="100px" title="{{ $doc['path'] }}"></img><br><br><span class="docname"><?=$name?></span></span>
                        @elseif (strpos($doc['path'], '.txt') !== false)
                            <span class="document"><img src="images/txt.jpg" width="100px" height="100px" title="{{ $doc['path'] }}"></img><br><br><span class="docname"><?=$name?></span></span>
                        @elseif (strpos($doc['path'], '.docx') !== false)
                            <span class="document"><img src="images/word.jpeg" width="100px" height="100px" title="{{ $doc['path'] }}"></img><br><br><span class="docname"><?=$name?></span></span>
                        @elseif (strpos($doc['path'], '.xlxs') !== false)
                            <span class="document"><img src="images/excel.png" width="100px" height="100px" title="{{ $doc['path'] }}"></img><br><br><span class="docname"><?=$name?></span></span>
                        @elseif (strpos($doc['path'], '.ppt') !== false)
                            <span class="document"><img src="images/powerpoint.jpg" width="100px" height="100px" title="{{ $doc['path'] }}"></img><br><br><span class="docname"><?=$name?></span></span>
                        @else
                            <span class="document"><img src="images/jpg.png" width="100px" height="100px" title="{{ $doc['path'] }}"></img><br><br><span class="docname"><?=$name?></span></span>
                        @endif
                    
                    @endforeach
                @endif
            </ul>
        </div>

    </div>

</div>

@endsection