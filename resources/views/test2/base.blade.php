
    <!DOCTYPE html>
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    
    <head>
        <base href="/">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <title>4US</title>
    
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    
         <!-- Styles -->
         <link href="{{asset('css/home.css')}}" rel="stylesheet">
         
         <!-- jQuery -->
         <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
         <script src="{{asset('js/home.js')}}"></script>

    </head>
    
    <body>
        <div class="flex-center full-height">
    
            <div id="menu_superior">
                <!-- <input id="caixa_pesquisa" placeholder="Procurar" type="text"/> -->
                <div id="lista">
                    <a href="/about"><span><i class="fa fa-pencil" aria-hidden="true"></i> <i class="textI">Editar Perfil</i></span></a>
                    <a href="/notifications"><span><i class="fa fa-globe" aria-hidden="true"></i> <i class="textI">Notificações</i></span></a>
                    <a href="{{ route('logout') }}"
                        onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        <span><i class="fa fa-sign-out" aria-hidden="true"></i> <i class="textI">Logout</i></span>
                    </a>
    
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </div>
            </div>
    
            <div id="barra_ladodireito">
                <a href="#">
                    <div id="imagem_utilizador"><img src={{$profile}} alt="{{$current_user->name}}"></div>
                </a>
                <h2 id="nome_utilizador"><a href="#" title={{$current_user->name}}>{{$current_user->name}}</a></h2>
                <p id="menu_utilizador">
                    <a href="/profile"><span><i class="fa fa-user" aria-hidden="true"></i>Meu Perfil</span></a>
                    <a href="/groups"><span><i class="fa fa-users" aria-hidden="true"></i>Grupos</span></a>
                    <a href="/group/page/1"><span><i class="fa fa-users" aria-hidden="true"></i>Ocorrências Universidade</span></a>
                    <a href="/friends"><span><i class="fa fa-user" aria-hidden="true"></i>Amigos</span></a>
                    <a href="/documents"><span><i class="fa fa-book" aria-hidden="true"></i>Documentos</span></a>
                    <a href="/photos"><span><i class="fa fa-photo" aria-hidden="true"></i>Fotos</span></a>
                    <!-- Apenas administradores, professores e funcionários podem pedir token -->
                    @if ($current_user->role_id == 1 || $current_user->role_id == 3 || $current_user->role_id == 4)
                        <a href="/api/token"><span><i class="fas fa-link" aria-hidden="true"></i>Pedir Token</span></a>
                    @endif
                </p>
            </div>

            @yield('content')
            
    </body>
    
    </html>