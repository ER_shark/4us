@extends('test2.base')

@section('content')

<div id="friends_list">

    <div id="menu">
        <br><br>
    </div>


    <!-- Pedidos de amizade recebidos -->
    <div id="scrollbar">
        
        <p>PEDIDOS DE AMIZADE RECEBIDOS</p>

        @if (count($friendRequests) == 0)
            <p>Não existem pedidos de amizade recebidos.</p>
        @else

            @foreach ($friendRequests as $request)
                
                <div class="friend">
                    <div class="friend_image">
                    <img src="{{$request['userImage']}}" alt="{{$request['userWhoSentRequest']['name']}}" class="photo_user">
                    </div>
                    <div class="friend_info">
                        <p><strong><a class="simpleHref" href='profile/{{ $request["userWhoSentRequest"]["user_name"] }}'>{{$request['userWhoSentRequest']['name']}}</a></strong></p>

                        <p class="chat-time">{{$request['userWhoSentRequest']['user_name']}}</p>
                        <p><?=substr($request['userWhoSentRequest']['description'],0, 70) . '...'?></p>
                    </div>
                    <div class="friend_chat">
                        <p>
                            <a href="friend/accept/{{ $request['id'] }}" title="Aceitar"><span class="accept_friend">Aceitar &gt;</span></a>
                        </p>
                        <p>
                            <a href="friend/decline/{{ $request['id'] }}" title="Recusar"><span class="decline_friend">Recusar &gt;</span></a>
                        </p>
                    </div>
                </div>

                @endforeach

        @endif

    </div>



    <!-- Pedidos de amizade enviados -->
    <div id="scrollbar">
        
        <p>PEDIDOS DE AMIZADE ENVIADOS</p>

        @if (count($sentRequests) == 0)
            <p>Não existem pedidos de amizade enviados.</p>
        @else

            @foreach ($sentRequests as $request)
                
                <div class="friend">
                    <div class="friend_image">
                    <img src="{{$request['userImage']}}" alt="{{$request['user']['name']}}" class="photo_user">
                    </div>
                    <div class="friend_info">
                    <p><strong><a class="simpleHref" href='profile/{{ $request["user"]["user_name"] }}'>{{$request['user']['name']}}</a></strong></p>

                        <p class="chat-time">{{$request['user']['user_name']}}</p>
                        <p><?=substr($request['user']['description'],0, 70) . '...'?></p>
                    </div>
                    <div class="friend_chat">
                        <p>
                            <a href="friend/cancel/{{ $request['id'] }}" title="Recusar"><span class="decline_friend">Cancelar &gt;</span></a>
                        </p>
                    </div>
                </div>

                @endforeach

        @endif

    </div>



</div>

@endsection