<strong><h3 style="color: red;">TOKEN PEDIDO</h3></strong>

<p>

<strong>Caro {{ $user }},</strong><br><br>
O token pedido é <strong>{{ $token }}</strong> e tem uma validade de 1 hora, terminando assim a <strong>{{ $validation_date }}</strong>.

<br><br>
Qualquer tentativa de acesso após essa data, será impedida, pelo que terá de criar um novo token.
<br>
Este token deve ser guardado a fim de não revelar dados confidenciais.

<br><br>
<strong> Com este token, pode obter as seguintes informações: </strong>
<ul>
    <li>/api/users/{token} - Buscar todos os utilizadores</li>
    <li>/api/publications/{userID}/{token} - Buscar publicações de um determinado utilizador</li>
    <li>/api/publications/{token} - Buscar todas as publicações</li>
    <li>/api/comments/p/{publicationID}/{token} - Buscar todos os comentários de uma determinada publicação</li>
    <li>/api/comments/u/{userID}/{token} - Buscar todos os comentários de um determinado utilizador</li>
    <li>/api/comments/{token} - Buscar todos os comentários</li>
    <li>/api/documents/p/{publicationID}/{token} - Buscar todos os documentos de uma determinada publicação</li>
    <li>/api/documents/u/{userID}/{token} - Buscar todos os documentos de um determinado utilizador</li>
    <li>/api/documents/{token} - Buscar todos os documentos</li>
    <li>/api/friends/{userID}/{token} - Buscar todos os amigos de um determinado utilizador</li>
    <li>/api/photos/{userID}/{token} - Buscar todas as fotos de um determinado utilizador</li>
    <li>/api/photos/{token} - Buscar todas as fotos</li>

</ul>
</p>

