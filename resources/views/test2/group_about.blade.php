@extends('test2.base')

@section('content')

<div id="about_section">

    <div id="menu">
        @include('menu_group')
    </div>

    <div id="info">
            
        <div class="card-post">

            <!-- Apenas pode mudar os dados do grupo, quem criou o grupo (o dono) -->
            @if ($ownerID == $current_user->id)
                <div class="alignCenter">
                    <a id="editProfileButton" href='#'>
                        <i class="fa fa-pencil"></i>
                    </a>
                </div>
            @endif


            <div id="showProfile">

                <table style="width: 80%; margin: 0 auto;">

                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>

                    <tr>
                        <td><strong>Nome</strong></td>
                        <td>{{ $group['name'] }}</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>

                    <tr>
                        <td><strong>Descrição</strong></td>
                        <td>{{ $group['description'] }}</td>
                    </tr>

                </table>


            </div>



            <div id="editProfile">

                <form method="POST" action="/group/edit">
                    <table style="width: 80%; margin: 0 auto;">

                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="group_id" value="{{ $group['id'] }}">

                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>

                        <tr>
                            <td><strong>Nome</strong></td>
                            <td><input class="input" type="text" name="name" value="{{ $group['name'] }}"/></td>
                        </tr>

                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>

                        <tr>
                            <td><strong>Descrição</strong></td>
                            <td><textarea class="input" name="description">{{ $group['description'] }}</textarea></td>
                        </tr>

                        <tr>
                            <td>&nbsp;</td>
                            <td><input id="saveProfileButton" type="submit" value="Save"></input></td>
                        </tr>
                        

                    </table>
                </form>

            </div>

            
          
        </div>

    </div>

</div>

@endsection