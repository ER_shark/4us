@extends('test2.base')

@section('content')



<!-- The Modal -->
<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="close">&times;</span>
    <p>Insira a(s) fotografia(s).</p>

    <form action="{{ route('file') }}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        <input type="file" name="files[]" accept="image/*" multiple>
        <button type="submit">Inserir fotografia(s)</button>
    </form>

  </div>

</div>


<div id="album_container">

    <div id="menu">
        @include('menu')
    </div>

    <div class="photo">
    
        <div class="alignCenter">
            <a id="addPhotoButton" href=''>
                <i class="fa fa-plus"></i>
            </a>
        </div><br><br>


        <div id="photos_album">
        
            @foreach ($photos as $photo)
            <div class="image">
                <img src="{{$photo['url']}}" alt="{{$myself->name}}">
            </div>
            @endforeach
         </div>
         
    </div>

</div>

@endsection