@extends('test2.base')

@section('content')


<!-- The Modal -->
<div id="groupModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="groupModalClose">&times;</span>
    <p>Criar novo grupo</p>

    <!-- Action tem a rota com o name createGroup -->
    <form action="{{ route('createGroup') }}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        <input style="width: 100%;" type="text" name="name" placeholder="Nome do Grupo">
        <textarea name="description" placeholder="Descrição do Gruoo"></textarea>
        <button type="submit">Criar Grupo</button>
    </form>

  </div>

</div>

<div id="friends_list">

    <div id="menu">
        @include('menu')
    </div>

    <div id="scrollbar">

        <!-- Apenas pode procurar grupos ou criar grupo caso esteja no seu próprio perfil -->
        <!-- Caso o perfil que esteja a ser visto seja o perfil do current_user -->
        @if ($myself->id == $current_user->id)

            <!-- Pode criar grupo se for professor, administrador ou funcionário -->
            @if ($current_user['role_id'] == 1 || $current_user['role_id']  == 3  || $current_user['role_id'] == 4) 
            <div class="alignCenter">
                <a id="addGroupButton" href=''>
                    <i class="fa fa-plus"></i>
                </a>
            </div>
            @endif


            <form method="post" action="{{ route('searchGroup') }}">

                {{ csrf_field() }}

                <div class="alignCenter">
                    <button id="searchGroupButton" type="submit"><i class="fa fa-search"></i></button>
                </div>

                <input id="groupSearchBox" name="search" type="text" placeholder="Pesquisar Grupo..."><br><br>
            </form>

        @endif
        
        <div id="groups_list" class="alignCenter">
            
            <!-- Vai mostrar os grupos que o utilizador está inscrito -->
            @foreach ($userGroups as $group)
            
                <div class="group">
                    <br>
                    <span class="fa fa-users"></span>

                    <p><strong>{{ $group['name'] }}</strong></p>
                    <p class="chat-time"><?=substr($group['description'],0, 120) . '...'?></p>
                    
                    <p><a href="group/page/{{ $group['id'] }}" title="Entrar no Grupo"><span class="join_group">Ver Grupo &gt;</span></a></p>

                </div>

            @endforeach



            <!-- Apenas vai mostrar sugestões caso esteja no seu próprio perfil -->
            <!-- Ou seja, caso o perfil que esteja a ser visto seja o perfil do current_user -->
            @if ($myself->id == $current_user->id)

                <!-- Vai mostrar os grupos que o utilizador não está inscrito (sugestões) -->
                @foreach ($suggestions as $suggestion)
                
                    <div class="group">
                        <br>
                        <span class="fa fa-users"></span>

                        <p><strong>{{ $suggestion['name'] }}</strong></p>
                        <p class="chat-time"><?=substr($suggestion['description'],0, 120) . '...'?></p>
                        
                        <p><a href="group/join/{{ $suggestion['id'] }}" title="Entrar no Grupo"><span class="join_group">Entrar no Grupo &gt;</span></a></p>

                    </div>

                @endforeach

            @endif


        </div>

       



    </div>

</div>

@endsection