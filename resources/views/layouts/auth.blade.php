<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>4US - @yield('title','Rede Social para Estudantes')</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link href="{{asset('css/auth.css')}}" rel="stylesheet">

    </head>
    <body>
        <div class="flex-center full-height">

            <div id="centro_pagina">
                @yield('content')
            </div>


            <footer>
                <center>
                    <div class="seccao_footer">
                        <a href="sobre.html">Sobre</a>
                    </div>
                    <div class="seccao_footer">
                        <a href="termos_uso.html">Termos de Uso</a>
                    </div>
                    <div class="seccao_footer">
                        <a href="termos_uso.html">Termos de Abuso</a>
                    </div>
                </center>
            </footer>


        </div>
    </body>
</html>
