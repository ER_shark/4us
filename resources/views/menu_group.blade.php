<html>
    <head>
    <link href="{{asset('css/home.css')}}" rel="stylesheet">
    </head>
    <body>
    
        <div id="banner"></div>

        <?php 
        function isActive($currentUrl) {
            $url = $_SERVER['REQUEST_URI'];
            
            if (strpos($url, $currentUrl) !== false) {
                return "class='active'";
            }

            return "";
        }
        ?>

        <ul id="profile_menu">
            <li <?=isActive('/group/page');?>><a href="<?=str_replace(explode("/", $_SERVER['REQUEST_URI'])[2], "page", $_SERVER['REQUEST_URI'])?>">Grupo</a></li>
            <li <?=isActive('/group/about');?>><a href="<?=str_replace(explode("/", $_SERVER['REQUEST_URI'])[2], "about", $_SERVER['REQUEST_URI'])?>">Sobre</a></li>
            <li <?=isActive('/group/members');?>><a href="<?=str_replace(explode("/", $_SERVER['REQUEST_URI'])[2], "members", $_SERVER['REQUEST_URI'])?>">Membros</a></li>
            <li <?=isActive('/group/documents');?>><a href="<?=str_replace(explode("/", $_SERVER['REQUEST_URI'])[2], "documents", $_SERVER['REQUEST_URI'])?>">Documentos</a></li>
        </ul>

    </body>
</html>